#ifndef OUTPUTOPTIONS_H
#define OUTPUTOPTIONS_H

#include <cmduserparams.h>

#include <QMetaType>
#include <QString>

class OutputOptions {
public:
    OutputOptions() = delete;

    enum Containers {
        AVI,
        MP4,
        MKV,
        SingleImages
    };

    enum ImageCodecs {
        PNG,
        JPEG
    };

    enum VideoCodecs {
        MJPEG,
        XVID,
        H264,
        H265
    };

    class Config {
    public:
        QString path;
        QString prefix;
        Containers container;
        ImageCodecs imageCodec;
        VideoCodecs videoCodec;
        int fps;
        bool wellsToDirs;
    };
};

Q_DECLARE_METATYPE(OutputOptions::Containers)
Q_DECLARE_METATYPE(OutputOptions::ImageCodecs)
Q_DECLARE_METATYPE(OutputOptions::VideoCodecs)
Q_DECLARE_METATYPE(OutputOptions::Config)

auto cmdCntToOpt(const CmdContainers c) -> OutputOptions::Containers;
auto cmdCodecToImg(const CmdCodecs c) -> OutputOptions::ImageCodecs;
auto cmdCodecToVid(const CmdCodecs c) -> OutputOptions::VideoCodecs;

#endif // OUTPUTOPTIONS_H
