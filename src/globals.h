#ifndef GLOBALS_H
#define GLOBALS_H

#include <QString>

class Globals {
public:
    Globals() = delete;

    static const QString APPLICATION_NAME;
    static const int VER_MAJ;
    static const int VER_MIN;
    static const char VER_REV;

    static auto versionString() -> QString;
};

#endif // GLOBALS_H
