#include "regiontablemodel.h"

#include <regionmanager.h>

#include <cassert>

enum Columns {
    ULX = 0,
    ULY = 1,
    BRX = 2,
    BRY = 3
};

RegionTableModel::RegionTableModel(RegionManager &mgr, QObject *parent) :
    QAbstractTableModel{parent},
    h_regionMgr{mgr},
    m_lastRegionCount{0}
{
    connect(&h_regionMgr, &RegionManager::regionsChanged, this, &RegionTableModel::onRegionsChanged);
}

auto RegionTableModel::columnCount(const QModelIndex &parent) const -> int
{
    if (parent.isValid())
        return 0;
    return 4;
}

auto RegionTableModel::data(const QModelIndex &idx, int role) const -> QVariant
{
    if (!idx.isValid())
        return {};
    if (!(role == Qt::DisplayRole || role == Qt::EditRole))
        return {};

    const auto &regions = h_regionMgr.regions();

    const auto row = idx.row();
    const auto col = idx.column();

    if (row < 0 || row >= rowCount() || col < 0 || col >= columnCount())
        return {};

    const auto &reg = regions.at(row);
    switch (col) {
    case ULX:
        return reg.topLeft().x();
    case ULY:
        return reg.topLeft().y();
    case BRX:
        return reg.bottomRight().x();
    case BRY:
        return reg.bottomRight().y();
    }

    return {};
}

auto RegionTableModel::flags(const QModelIndex &idx) const -> Qt::ItemFlags
{
    if (!idx.isValid())
        return Qt::NoItemFlags;

    const int row = idx.row();
    const int col = idx.column();
    if (row < 0 || row >= rowCount() || col < 0 || col >= columnCount())
        return Qt::NoItemFlags;

    return Qt::ItemIsEditable | Qt::ItemIsEnabled | Qt::ItemIsSelectable;
}

auto RegionTableModel::headerData(int section, Qt::Orientation orientation, int role) const -> QVariant
{
    if (!(role == Qt::DisplayRole || role == Qt::EditRole))
        return {};

    if (section < 0)
        return {};

    switch (orientation) {
    case Qt::Horizontal:
        switch (section) {
        case ULX:
            return "ULX";
        case ULY:
            return "ULY";
        case BRX:
            return "BRX";
        case BRY:
            return "BRY";
        default:
            return {};
        }
    case Qt::Vertical:
        return section < h_regionMgr.regions().size() ? section + 1: QVariant{};
    }

    return {};
}

auto RegionTableModel::rowCount(const QModelIndex &parent) const -> int
{
    if (parent.isValid())
        return 0;
    return h_regionMgr.regions().size();
}

void RegionTableModel::onRegionsChanged(const QVector<QRect> &regions, const QVector<int> &changed)
{
    if (changed.size() == 0) {
        beginResetModel();
        m_lastRegionCount = regions.size();
        endResetModel();
        return;
    }

    assert(changed[0] <= m_lastRegionCount);

    // New region has been added
    if (changed[0] == m_lastRegionCount) {
        beginInsertRows({}, m_lastRegionCount, m_lastRegionCount);
        m_lastRegionCount = regions.size();
        endInsertRows();
        return;
    }

    // Regions has been removed
    // We only support removing one region at a time
    if (changed[0] <= 0) {
        const auto row = -changed[0];
        beginRemoveRows({}, row, row);
        m_lastRegionCount--;
        assert(m_lastRegionCount >= 0);
        endRemoveRows();
        return;
    }

    int first{-1}, last{-1};
    // Some regions have changed
    for (const auto &idx : changed) {
        assert(idx >= 0);
        if (idx < first && first > 0)
            first = idx;
        if (idx > last)
            last = idx;
    }

    emit dataChanged(index(first, 0), index(last, 3), { Qt::DisplayRole });
}

auto RegionTableModel::setData(const QModelIndex &idx, const QVariant &value, int role) -> bool
{
    if (!idx.isValid())
        return false;
    if (role != Qt::EditRole)
        return false;

    bool ok;
    auto coord = value.toInt(&ok);
    if (!ok)
        return false;

    const int row = idx.row();
    const int col = idx.column();

    if (row < 0 || row >= rowCount() || col < 0 || col >= columnCount())
        return false;

    auto reg = h_regionMgr.regions()[row];
    switch (col) {
    case ULX:
        reg.setLeft(coord);
        break;
    case ULY:
        reg.setTop(coord);
        break;
    case BRX:
        reg.setRight(coord);
        break;
    case BRY:
        reg.setBottom(coord);
        break;
    default:
        return false;
    }

    return h_regionMgr.updateRegion(row, reg);
}
