#include "opencvvideo.h"

#include <opencv2/opencv.hpp>

#include <QtConcurrent/QtConcurrent>

#include <cmath>

static const char MJPG[4] = {'M', 'J', 'P', 'G'};
static const char XVID[4] = {'X', 'V', 'I', 'D'};
static const char H264[4] = {'m', 'p', '4', 'v'};
static const char HEVC[4] = {'H', 'E', 'V', 'C'};

inline
auto FourCC(const char code[4])
{
    return *reinterpret_cast<const int*>(&code[0]);
}

inline
auto maxDigits(const uint_fast64_t d)
{
    return int(std::floor(std::log10(d))) + 1;
}

inline
auto paddedNum(const int num, const int digits)
{
    return QString{"%1"}.arg(num, digits, 10, QChar{'0'});
}

inline
auto numToWell(const int num, const int maxPadding = 2)
{
    return paddedNum(num+1, maxPadding);
}

inline
auto findControlToken(const int num, QString str, const int from, const std::vector<QChar> &ctrlChars) -> std::tuple<int, QString, char, int>
{
    QString prefix{};
    int idx{from};
    int tokenStart{from};
    char id{0};
    bool inToken{false};
    bool ignoreNext{false};
    int LEN = str.length();
    for (; idx < LEN; idx++) {
        if (ignoreNext) {
            ignoreNext = false;
            continue;
        }

        const auto &ch = str[idx];
        if (ch == '\\') {
            if (inToken)
                throw OpenCVVideo::Error{"Invalid nameing scheme"};

            str = str.remove(idx, 1);
            LEN--;
            ignoreNext = true;
            continue;
        }
        if (ch == '%') {
            inToken = true;
            tokenStart = idx + 1;
            continue;
        }

        if (!inToken)
            continue;

        if (ch.isDigit())
            prefix.append(ch);
        else if (std::find_if(ctrlChars.cbegin(), ctrlChars.cend(), [ch](auto v) { return v == ch; }) != ctrlChars.cend()) {
            id = ch.toLatin1();
            break;
        } else {
            inToken = false;
            prefix.clear();
        }
    }

    if (idx == LEN) // No valid token
        return {-1, std::move(str), 0, 0};

    int pad{-1};
    bool ok;
    if (!prefix.isEmpty()) {
        pad = prefix.toInt(&ok);
        if (!ok)
            throw OpenCVVideo::Error{"Invalid naming scheme"};
    }

    auto processed = str.replace(tokenStart, idx - tokenStart + 1, QString::number(num + 1));
    return {
        tokenStart,
        std::move(processed),
        id,
        pad
    };
}

inline
auto parseScheme(QString scheme, const int numWells, const int numFrames) -> std::tuple<QString, std::vector<std::function<QString (const QString &, int, int)>>>
{
    std::vector<std::function<QString (const QString &, int, int)>> formatters{};
    int from{0};
    int counter{0};
    while (true) {
        auto ret = findControlToken(counter++, scheme, from, { 'w', 'f' });
        from = std::get<0>(ret);
        if (from < 0)
            break;

        scheme = std::move(std::get<1>(ret));
        auto id = std::get<2>(ret);
        auto pad = std::get<3>(ret);

        if (id == 'w') {
            const auto _pad = pad == -1 ? maxDigits(numWells) : pad;
            formatters.emplace_back([_pad](const QString &str, const int well, const int frame) {
                (void)frame;
                return str.arg(numToWell(well, _pad));
            });
        } else if (id == 'f') {
            const auto _pad = pad == -1 ? maxDigits(numFrames) : pad;
            formatters.emplace_back([_pad](const QString &str, const int well, const int frame) {
                (void)well;
                return str.arg(paddedNum(frame, _pad));
            });
        } else
            throw OpenCVVideo::Error{"Invalid formatter id"};
    }

    if (formatters.size() > 9)
        throw new OpenCVVideo::Error{"Maximum of 9 formatting parameters is supported"};

    return { scheme, formatters };
}

inline
auto makeImageNamer(QString scheme, const int numWells, const int numFrames, const QString &suffix) -> std::function<QString (const int, const int)>
{
    if (scheme.isEmpty()) { // No user-defined scheme
        const auto padWells = maxDigits(numWells);
        const auto padFrames = maxDigits(numFrames);

        return [padWells, padFrames, suffix](const int well, const int frame) {
            return QString{"%1_%2.%3"}.arg(numToWell(well, padWells)).arg(paddedNum(frame, padFrames)).arg(suffix);
        };
    }

    auto parsed = parseScheme(scheme, numWells, numFrames);
    scheme = std::move(std::get<0>(parsed));
    auto formatters = std::move(std::get<1>(parsed));

    return [scheme, formatters, suffix](const int well, const int frame) -> QString {
        QString out{scheme};
        for (const auto &fmtr : formatters)
            out = fmtr(out, well, frame);
        return out + "." + suffix;
    };
}

inline
auto makeVideoNamer(QString scheme, const QString &path, const int numWells, const int numFrames, const QString suffix) -> std::function<QString (const int, const int)>
{
    if (scheme.isEmpty()) { // Empty scheme, use predefined template
        auto tmpl = path + QString{"/well_%1."} + suffix;
        auto pad = maxDigits(numWells);
        return [tmpl, pad](const int well, const int frame) -> QString {
            (void)frame;
            return tmpl.arg(numToWell(well, pad));
        };
    }

    auto parsed = parseScheme(scheme, numWells, numFrames);
    scheme = std::move(std::get<0>(parsed));
    auto formatters = std::move(std::get<1>(parsed));

    return [path, scheme, formatters, suffix](const int well, const int frame) -> QString {
        QString out{scheme};
        for (const auto &fmtr : formatters)
            out = fmtr(out, well, frame);
        return path + "/" + out + "." + suffix;
    };
}

OpenCVVideo::OpenCVVideo() :
    m_numFrames{0}
{}

OpenCVVideo::OpenCVVideo(const QString &path) :
    m_source{path.toLocal8Bit().data()},
    m_numFrames{0}
{
    if (!m_source.isOpened())
        throw Error{"Cannot open video file"};

    m_numFrames = m_source.get(cv::CAP_PROP_FRAME_COUNT);
    if (m_numFrames < 1)
        throw Error{"Video contains no frames or the number of frames information cannot be read"};

    m_width = m_source.get(cv::CAP_PROP_FRAME_WIDTH);
    if (m_width < 1)
        throw Error{"Invalid video width"};

    m_height = m_source.get(cv::CAP_PROP_FRAME_HEIGHT);
    if (m_height < 1)
        throw Error{"Invalid video height"};

    m_fps = m_source.get(cv::CAP_PROP_FPS);
    if (m_fps <= 0)
        m_fps = 50.0;
}

auto OpenCVVideo::frame(const uint_fast64_t idx) -> VideoFrame
{
    auto buf = frameRaw(idx);
    if (buf.empty())
        return {std::move(buf)};

    cv::cvtColor(buf, buf, cv::COLOR_BGR2RGB);

    return {std::move(buf)};
}

auto OpenCVVideo::frameRaw(const uint_fast64_t idx) -> cv::Mat
{
    cv::Mat buf{};

    if (!m_source.isOpened())
        return buf;

    if (idx >= m_numFrames)
        return buf;

    m_source.set(cv::CAP_PROP_POS_FRAMES, idx);
    if (!m_source.read(buf))
        return cv::Mat{};

    return buf;
}

auto OpenCVVideo::frameSize() -> QSize
{
    if (!m_source.isOpened())
        return {};

    cv::Mat buf{};
    if (!m_source.read(buf))
        return {};
    return {buf.cols, buf.rows};
}

auto OpenCVVideo::isValid() const -> bool
{
    return m_numFrames > 0 && m_source.isOpened();
}

auto OpenCVVideo::numFrames() const -> uint_fast64_t
{
    return m_numFrames;
}

auto OpenCVVideo::splitToImages(const QString &path, const QString &scheme, const OutputOptions::ImageCodecs codec, const bool wellsToDirs, const QVector<QRect> &regions) -> void
{
    QString suffix{};
    std::vector<int> params{};

    switch (codec) {
    case OutputOptions::JPEG:
        suffix = "jpg";
        params.emplace_back(cv::IMWRITE_JPEG_QUALITY);
        params.emplace_back(100);
        break;
    case OutputOptions::PNG:
        suffix = "png";
        params.emplace_back(cv::IMWRITE_PNG_COMPRESSION);
        params.emplace_back(4);
        break;
    default:
        throw Error{"Invalid image format"};
    }

    QDir basePath{path};
    if (!basePath.mkpath(path))
        throw new Error{"Cannot create base output directory"};

    const int padWells = maxDigits(regions.size());
    if (wellsToDirs) {
        for (int idx{0}; idx < regions.size(); idx++) {
            const auto well = numToWell(idx, padWells);
            QDir dir{basePath.absolutePath() + "/" + well};
            if (dir.exists(well))
                throw Error{QString{"Directory %1/%2 already exists. Please remove it manually."}.arg(basePath.absolutePath()).arg(well)};
            if (!basePath.mkdir(well))
                throw Error{"Failed to create subdirectories for wells"};
        }
    }

    m_source.set(cv::CAP_PROP_POS_FRAMES, 0);

    const auto namer = makeImageNamer(scheme, regions.size(), m_numFrames, suffix);

    QVector<std::tuple<int, QString, cv::Rect>> workingSets{};
    for (auto idx{0}; idx < regions.size(); idx++) {
        const auto &reg = regions[idx];
        cv::Rect window{reg.left(), reg.top(), reg.width(), reg.height()};
        auto p = [&]() {
            if (wellsToDirs)
                return path + "/" + numToWell(idx, padWells);
            return path;
        }();
        workingSets.push_back({idx, p, window});
    }

    int frameCounter{1};
    cv::Mat frame{};
    while (m_source.read(frame)) {
        const auto encode = [&params, &frame, &namer, frameCounter](const auto set) {
            const auto idx = std::get<0>(set);
            const auto &dirPath = std::get<1>(set);
            const auto &window = std::get<2>(set);
            const QString outputPath = dirPath + "/" + namer(idx, frameCounter);

            const auto cropped = frame(window);
            if (!cv::imwrite(outputPath.toStdString(), cropped, params))
                throw Error{"Failed to write image"};
        };

        QtConcurrent::map(workingSets, encode).waitForFinished();
        frameCounter++;
    }
}

auto OpenCVVideo::splitToVideos(const QString &path, const QString &scheme, const OutputOptions::Containers cnt, const OutputOptions::VideoCodecs codec, const int fps, const QVector<QRect> &regions) -> void
{
    const auto suffix = [cnt]() {
        switch (cnt) {
        case OutputOptions::AVI:
            return "avi";
        case OutputOptions::MP4:
            return "mp4";
        case OutputOptions::MKV:
            return "mkv";
        default:
            throw Error{"Invalid container"};
        }
        throw Error{"Unknown container"};
    }();

    const auto fourcc = [codec]() {
        switch (codec) {
        case OutputOptions::MJPEG:
            return FourCC(MJPG);
        case OutputOptions::XVID:
            return FourCC(XVID);
        case OutputOptions::H264:
            return FourCC(H264);
        case OutputOptions::H265:
            return FourCC(HEVC);
        }
        throw Error{"Unknown codec"};
    }();

    QDir dir{path};
    if (!dir.mkpath(path))
        throw Error{"Failed to create output directory"};

    const auto usedFps = (fps < 0) ? m_fps : fps;

    const auto namer = makeVideoNamer(scheme, path, regions.size(), m_numFrames, suffix);

    QVector<QPair<cv::VideoWriter *, cv::Rect>> workingSets{};
    for (auto idx{0}; idx < regions.size(); idx++) {
        const QRect &reg = regions[idx];
        const QString outputFile = namer(idx, 0);
        cv::Size size{reg.width(), reg.height()};
        cv::Rect window{reg.left(), reg.top(), reg.width(), reg.height()};

        auto writer = new cv::VideoWriter(outputFile.toStdString(), fourcc, usedFps, size, true);
        workingSets.push_back({writer, window});
    }

    m_source.set(cv::CAP_PROP_POS_FRAMES, 0);

    cv::Mat frame{};
    while (m_source.read(frame)) {
        const auto encoder = [&frame](const auto set) {
            const auto writer = set.first;
            const auto &window = set.second;
            auto cropped = frame(window);
            writer->write(cropped);
        };

        try {
            QtConcurrent::map(workingSets, encoder).waitForFinished();
        } catch (const QException &ex) {
            for (auto &set : workingSets)
                delete set.first;
            throw ex;
        }
    };

    for (auto &set : workingSets)
        delete set.first;
}
