#include "videoframe.h"

#include <regionmanager.h>

#include <opencv2/imgproc.hpp>

#define ALPHA 0.25
#define R_ALPHA 1 - ALPHA

inline
auto addRegion(const QRect &reg, const cv::Scalar clr, const int idx, cv::Mat &output)
{
    cv::Point tl{reg.left(), reg.top()};
    cv::Point br{reg.right(), reg.bottom()};
    cv::rectangle(output, tl, br, clr, 4, cv::LINE_AA);
    cv::putText(output, std::to_string(idx), {tl.x + 15, tl.y + 75}, cv::FONT_HERSHEY_SIMPLEX, 3, clr, 4, cv::LINE_AA);
}

VideoFrame::VideoFrame()
{
}

VideoFrame::VideoFrame(cv::Mat &&data) :
    m_sourceData{std::move(data)}
{
    m_sourceData.copyTo(m_outputData);
}

auto VideoFrame::image(const RegionManager &mgr) const -> QImage
{
    const int width = m_sourceData.cols;
    const int height = m_sourceData.rows;

    if (width > 0 && height > 0) {
        // Buffer should be in RGB24
        const auto regionBorders = overlayRegions(mgr);
        cv::addWeighted(m_sourceData, ALPHA, regionBorders, R_ALPHA, 0, m_outputData);

        return QImage{m_outputData.data, width, height, QImage::Format_RGB888};
    }
    return {};
}

auto VideoFrame::overlayRegions(const RegionManager &mgr) const -> cv::Mat
{
    cv::Mat regionBorders{};
    m_sourceData.copyTo(regionBorders);

    // Selected regions
    int ctr{1};
    for (const auto &reg : mgr.regions())
        addRegion(reg, cv::Scalar(19, 19, 255), ctr++, regionBorders);

    // Provisional region (if any)
    const auto &prov = mgr.provisionalRegion();
    if (!prov.isEmpty())
        addRegion(prov, cv::Scalar(50, 255, 50), ctr, regionBorders);

    return regionBorders;
}
