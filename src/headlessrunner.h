#ifndef HEADLESSRUNNER_H
#define HEADLESSRUNNER_H

#include <cmduserparams.h>

auto runHeadless(const CmdUserParams &userParams) -> int;

#endif // HEADLESSRUNNER_H
