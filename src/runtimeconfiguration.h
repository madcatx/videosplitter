#ifndef RUNTIMECONFIGURATION_H
#define RUNTIMECONFIGURATION_H

#include <persistentsettings.h>

#include <QSettings>
#include <QString>

template <typename T>
class ConfigOption {
public:
    ConfigOption() :
        hasValue{false}
    {}

    bool hasValue;
    T value;
};

#define DECLARE_CFGOPT(type, name, default_value, persistent_id) \
    private: \
        ConfigOption<type> m_##name; \
    public: \
        auto name () { \
            if (!m_##name.hasValue) { \
                auto var = PersistentSettings::get()->value(persistent_id); \
                if (var.canConvert<type>()) \
                    m_##name.value = var.value<type>(); \
                else \
                    m_##name.value = default_value; \
                m_##name.hasValue = true; \
            } \
            return m_##name.value; \
        } \
        auto set_##name(type v) { \
            m_##name.value = std::move(v); \
            m_##name.hasValue = true; \
            PersistentSettings::get()->setValue(persistent_id, m_##name.value); \
        }

class RuntimeConfiguration {
    DECLARE_CFGOPT(double, finderMaxAreaThreshold, 10.0, "finder/max_area_threshold");
    DECLARE_CFGOPT(double, finderMinAreaThreshold, 2.5, "finder/min_area_threshold");
    DECLARE_CFGOPT(double, finderScaleFactor, 3.0, "finder/scale_factor");
    DECLARE_CFGOPT(QString, python3Path, "python3", "global/python3Path");
    DECLARE_CFGOPT(QString, inferPositionsScriptPath, "", "infer/positions_script_path");
    DECLARE_CFGOPT(QString, inferPositionsModelPath, "", "infer/positions_model_path");

public:
    static auto get() -> RuntimeConfiguration *;

private:
    RuntimeConfiguration();

    static RuntimeConfiguration *s_me;
};

#endif // RUNTIMECONFIGURATION_H
