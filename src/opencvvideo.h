#ifndef OPENCVVIDEO_H
#define OPENCVVIDEO_H

#include <outputoptions.h>
#include <videoframe.h>

#include <opencv2/core.hpp>
#include <opencv2/video.hpp>
#include <opencv2/videoio.hpp>

#include <QException>
#include <QImage>
#include <QString>

#include <stdexcept>

class OpenCVVideo {
public:
    class Error : public QException {
    public:
        Error(QString msg) :
            QException{},
            m_what{std::move(msg)}
        {}

        void raise() const { throw *this; }
        Error *clone() const { return new Error{*this}; }

        auto what() const noexcept -> const char *
        {
            return m_what.toLocal8Bit();
        }
    private:
        QString m_what;
    };

    OpenCVVideo();
    OpenCVVideo(const QString &path);
    auto frame(const uint_fast64_t idx) -> VideoFrame;
    auto frameRaw(const uint_fast64_t idx) -> cv::Mat;
    auto frameSize() -> QSize;
    auto isValid() const -> bool;
    auto numFrames() const -> uint_fast64_t;
    auto splitToImages(const QString &path, const QString &scheme, const OutputOptions::ImageCodecs codec, const bool wellsToDirs, const QVector<QRect> &regions) -> void;
    auto splitToVideos(const QString &path, const QString &scheme, const OutputOptions::Containers cnt, const OutputOptions::VideoCodecs codec, const int fps, const QVector<QRect> &regions) -> void;

private:
    cv::VideoCapture m_source;
    uint_fast64_t m_numFrames;
    int m_width;
    int m_height;
    double m_fps;
};

#endif // OPENCVVIDEO_H
