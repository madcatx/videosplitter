#ifndef CMDLINEPARSER_H
#define CMDLINEPARSER_H

#include <cmduserparams.h>

#include <QCommandLineParser>

class QApplication;

class CmdLineParser {
public:
    static auto process(QApplication &app) -> CmdUserParams;

private:
    explicit CmdLineParser();

    QCommandLineParser m_parser;

    static CmdLineParser *s_me;
};

#endif // CMDLINEPARSER_H
