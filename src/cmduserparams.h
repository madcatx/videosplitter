#ifndef CMDUSERPARAMS_H
#define CMDUSERPARAMS_H

#include <QString>

#define _X_MK_ENUM(name, ...) \
    enum class name { \
        __VA_ARGS__ \
    }
#define _X_MK_ENUM_NAMES_DECL(name) extern const char *name##Names[]
#define _X_MK_ENUM_NAMES_DEF(name, ...) const char *name##Names[] = { #__VA_ARGS__ }
#define MK_PRINTABLE_ENUM(name, ...) \
    _X_MK_ENUM(name, __VA_ARGS__); \
    _X_MK_ENUM_NAMES_DECL(name)

MK_PRINTABLE_ENUM(CmdContainers, AVI, MP4, MKV, SingleImages);
MK_PRINTABLE_ENUM(CmdCodecs, JPEG, PNG, MJPEG, XVID, H264, H265);

template <typename T>
class MaybeParam {
public:
    MaybeParam() :
        m_isSet{false}
    {}

    auto isSet() const
    {
        return m_isSet;
    }

    auto set(T t)
    {
        m_value = std::move(t);
        m_isSet = true;
    }

    auto operator=(T t)
    {
        set(t);

        return *this;
    }

    auto operator=(const MaybeParam<T> &other)
    {
        m_value = other.m_value;
        m_isSet = other.m_isSet;
    }

    const T & operator()() const {
        return m_value;
    }

private:
    bool m_isSet;
    T m_value;
};

class CmdUserParams {
public:
    bool headless;                        /*!< Run the application in headless batch mode */
    bool wellsToDirs;                     /*!< Use wellsToDirs flag in video processing */
    MaybeParam<double> arfMaxAreaThr;     /*!< Automatic region finder maximum area threshold (percent) */
    MaybeParam<double> arfMinAreaThr;     /*!< Automatic region finder minimum area threshold (percent) */
    MaybeParam<double> arfScaleFactor;    /*!< Automatic region finder scale factor (percent) */
    MaybeParam<CmdCodecs> codec;          /*!< Codec */
    MaybeParam<CmdContainers> container;  /*!< Container */
    MaybeParam<double> fps;               /*!< Expected frameratae of the source video. Negative value means autodetect */
    QString input;                        /*!< Input video file */
    QString namingScheme;                 /*!< Naming scheme for output files */
    QString output;                       /*!< Output path */
    QString regions;                      /*!< Regions file */
    bool posInf;                          /*!< Run position inference script after splitting */
    QString posInfModel;                  /*!< Path to position inference tool neural net model data */
    QString posInfOutput;                 /*!< Path where to store position inference script output. Empty means store it with the splitted source data */
    QString posInfScript;                 /*!< Path to position inference script */
    QString python3Path;                  /*!< Path to Python 3 installation. Empty means try to use system Python 3 installation */
};

auto cmdCodecIsVideo(const CmdCodecs c) -> bool;
auto stringToCmdCodec(const QString &str) -> CmdCodecs;
auto stringToCmdContainer(const QString &str) -> CmdContainers;

#endif // CMDUSERPARAMS_H
