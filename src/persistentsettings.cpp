#include "persistentsettings.h"

#include <QSettings>

#ifdef Q_OS_WIN
#include <QApplication>
#endif // Q_OS_WIN

QSettings * PersistentSettings::s_settings{nullptr};

auto PersistentSettings::get() -> QSettings *
{
    if (s_settings == nullptr) {
    #ifndef Q_OS_WIN
        s_settings = new QSettings{}; // Use native format
    #else
        auto cfgPath = QString{"%1/VideoSplitter.ini"}.arg(qApp->applicationDirPath());
        s_settings = new QSettings{cfgPath, QSettings::IniFormat};
    #endif // Q_OS_WIN
    }
    return s_settings;
}

