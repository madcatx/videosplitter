#include "outputoptions.h"

#include <cassert>

#define CASE_CMDCNT_OPT(c) case CmdContainers::c: return OutputOptions::c
#define CASE_CMDCODEC_OPT(c) case CmdCodecs::c: return OutputOptions::c

auto cmdCntToOpt(const CmdContainers c) -> OutputOptions::Containers
{
    switch (c) {
    CASE_CMDCNT_OPT(AVI);
    CASE_CMDCNT_OPT(MP4);
    CASE_CMDCNT_OPT(MKV);
    CASE_CMDCNT_OPT(SingleImages);
    default:
        assert(false);
    }
}

auto cmdCodecToImg(const CmdCodecs c) -> OutputOptions::ImageCodecs
{
    switch (c) {
    CASE_CMDCODEC_OPT(JPEG);
    CASE_CMDCODEC_OPT(PNG);
    default:
        assert(false);
    }
}

auto cmdCodecToVid(const CmdCodecs c) -> OutputOptions::VideoCodecs
{
    switch (c) {
    CASE_CMDCODEC_OPT(MJPEG);
    CASE_CMDCODEC_OPT(XVID);
    CASE_CMDCODEC_OPT(H264);
    CASE_CMDCODEC_OPT(H265);
    default:
        assert(false);
    }
}
