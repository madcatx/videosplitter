#ifndef VIDEOFRAME_H
#define VIDEOFRAME_H

#include <opencv2/core.hpp>

#include <QFrame>

class RegionManager;

class VideoFrame {
public:
    explicit VideoFrame();
    VideoFrame(cv::Mat &&data);
    auto image(const RegionManager &mgr) const -> QImage;

private:
    auto overlayRegions(const RegionManager &mgr) const -> cv::Mat;

    cv::Mat m_sourceData;
    cv::Mat m_outputData;
};

#endif // VIDEOFRAME_H
