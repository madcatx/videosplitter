#include "positioninferencerunner.h"

#include <QDir>

PositionInferenceRunner::PositionInferenceRunner(QString imagesPath, QString outputPath, QString scriptPath, QString modelPath, QString python3Path) :
    m_imagesPath{std::move(imagesPath)},
    m_modelPath{std::move(modelPath)},
    m_outputPath{std::move(outputPath)},
    m_scriptPath{std::move(scriptPath)}
{
    if (python3Path.isEmpty())
        m_python3Path = "python3";
    else
        m_python3Path = std::move(python3Path);
}

auto PositionInferenceRunner::subdirs() -> QProcess::ExitStatus
{
    const auto outputToImgsDir = m_outputPath.isEmpty();

    QDir baseDir{m_imagesPath};
    auto subdirs = baseDir.entryInfoList(QDir::Dirs | QDir::NoDotAndDotDot);

    for (const auto &dir : subdirs) {
        QProcess proc{};

        const auto outputPath = [&]() {
            if (outputToImgsDir)
                return QString{"%1/%2"}.arg(dir.absoluteFilePath()).arg("eval_fish.csv");
            return QString{"%1/eval_fish_%2.csv"}.arg(m_outputPath).arg(dir.fileName());
        }();

        QStringList args{
            m_scriptPath,
             "infer-dir-dataframe",
            m_modelPath,
            dir.absoluteFilePath(),
            outputPath};

        proc.start(m_python3Path, args);
        proc.waitForFinished(-1);

        if (proc.exitStatus() != QProcess::NormalExit)
            return proc.exitStatus();
    }
    return QProcess::NormalExit;
}

auto PositionInferenceRunner::singleDir() -> QProcess::ExitStatus {
    QProcess proc{};

    const auto outputToImgsDir = m_outputPath.isEmpty();

    const auto outputPath = [&]() {
        if (outputToImgsDir)
            return QString{"%1/%2"}.arg(m_imagesPath).arg("eval_fish.csv");
        return QString{"%1/eval_fish.csv"}.arg(m_outputPath);
    }();

    QStringList args{
        m_scriptPath,
         "infer-dir-dataframe",
         m_modelPath,
         m_imagesPath,
         outputPath};

    proc.start(m_python3Path, args);
    proc.waitForFinished(-1);

    return proc.exitStatus();
};
