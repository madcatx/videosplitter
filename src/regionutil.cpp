#include "regionutil.h"

#include <opencv2/imgproc.hpp>

#include <cassert>
#include <cmath>
#include <vector>

#define BW_THRESH 127

namespace RegionUtil {

auto find(const cv::Mat &src, const double minAreaThreshold, const double maxAreaThreshold, const double scaleFactor) -> QVector<QRect>
{
    assert(minAreaThreshold > 0.0 && minAreaThreshold < 1.0);
    assert(scaleFactor > -1.0 && scaleFactor < 1.0);

    cv::Mat buf{};

    cv::cvtColor(src, buf, cv::COLOR_BGR2GRAY);
    cv::threshold(buf, buf, BW_THRESH, 255, 0);

    const auto entireArea = buf.rows * buf.cols;

    std::vector<std::vector<cv::Point>> contours{};
    std::vector<cv::Vec4i> hierarchy{};
    cv::findContours(buf, contours, hierarchy, cv::RETR_TREE, cv::CHAIN_APPROX_NONE);

    QVector<QRect> acceptedRegions{};
    const auto maxAcceptThreshold = entireArea * maxAreaThreshold;
    const auto minAcceptThreshold = entireArea * minAreaThreshold;
    for (const auto &cont : contours) {
        const auto area = cv::contourArea(cont);
        if (area > minAcceptThreshold && area < maxAcceptThreshold) {
            auto br = cv::boundingRect(cont);
            const int wx = std::round(br.width  * scaleFactor / 2.0);
            const int wy = std::round(br.height * scaleFactor / 2.0);
            acceptedRegions.push_back({br.x - wx, br.y -wy, br.width + 2 * wx, br.height + 2 * wy});
        }
    }

    std::sort(acceptedRegions.begin(), acceptedRegions.end(), sort);

    return acceptedRegions;
}

auto sort(const QRect &first, const QRect &second) -> bool
{
    const auto  TOL = first.height() * 0.25;
    const auto tL = first.top();
    const auto tR = second.top();
    const auto tLow = tL - TOL;
    const auto tHigh = tL + TOL;
    if (tR < tLow) // Second clearly above first
        return false;
    else if (tR > tHigh) // Second is clearly below first
        return true;
    // First and second are at ~ the same height - use horizontal position to decide
    return first.left() < second.left();
}

} // namespace RegionUtil
