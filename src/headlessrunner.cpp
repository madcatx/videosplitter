#include "headlessrunner.h"

#include <opencvvideo.h>
#include <outputoptions.h>
#include <positioninferencerunner.h>
#include <regionmanager.h>
#include <regionutil.h>

#include <QFileInfo>

#include <iostream>

static
auto prnInfo(const std::string &msg)
{
    std::cout << "INFO: " << msg << std::endl;
}

static
auto prnErr(const std::string &msg)
{
    std::cout << "ERROR: " << msg << std::endl;
}

static
auto checkPosInfParams(const CmdUserParams &params)
{
    if (params.container() != CmdContainers::SingleImages) {
        prnErr("Position inference script require videos to be split into images");
        return false;
    }

    if (params.posInfScript.isEmpty()) {
        prnErr("No path to position inference script was set");
        return false;
    }
    if (!QFileInfo{params.posInfScript}.exists()) {
        prnErr("Position inference script does not exist");
        return false;
    }

    if (params.posInfModel.isEmpty()) {
        prnErr("No position inference neural net model was set");
        return false;
    }
    if (!QFileInfo{params.posInfModel}.exists()) {
        prnErr("Position inference neural net model file does not exist");
        return false;
    }

    if (!params.python3Path.isEmpty() && !QFileInfo{params.python3Path}.exists()) {
        prnErr("Path to Python 3 installation does not exist");
        return false;
    }

    return true;
}

auto runHeadless(const CmdUserParams &params) -> int
{
    try {
        prnInfo("Checking parameters");

        OpenCVVideo video{params.input};
        if (!video.isValid()) {
            prnErr("Video file is not valid");
            return EXIT_FAILURE;
        }

        RegionManager regMgr{};
        if (params.regions.isEmpty()) {
            auto frame = video.frameRaw(0);
            if (frame.empty()) {
                prnErr("Empty video frame");
                return EXIT_FAILURE;
            }

            if (!params.arfMinAreaThr.isSet()) {
                prnErr("Minimum region area is not set");
                return EXIT_FAILURE;
            }
            if (!params.arfMaxAreaThr.isSet()) {
                prnErr("Maximum region area is not set");
                return EXIT_FAILURE;
            }
            if (!params.arfScaleFactor.isSet()) {
                prnErr("Region of scale factor is not set");
                return EXIT_FAILURE;
            }

            auto min = params.arfMinAreaThr();
            auto max = params.arfMaxAreaThr();
            auto sf = params.arfScaleFactor();

            if (!within(REGION_MIN_AREA, REGION_MAX_AREA, max)) {
                prnErr("Maximum region area is invalid");
                return EXIT_FAILURE;
            }
            if (!within(REGION_MIN_AREA, REGION_MAX_AREA, min)) {
                prnErr("Minimum region area is invalid");
                return EXIT_FAILURE;
            }
            if (!within(REGION_MIN_SCALE_FACTOR, REGION_MAX_SCALE_FACTOR, sf)) {
                prnErr("Region scale factor is invalid");
                return EXIT_FAILURE;
            }
            if (min >= max) {
                prnErr("Mismatching minimum and maximum region areas");
                return EXIT_FAILURE;
            }

            auto regs = RegionUtil::find(frame, min / 100.0, max / 100.0, sf / 100.0);
            if (regs.empty()) {
                prnErr("No regions were found");
                return EXIT_FAILURE;
            }
            regMgr.setRegions(std::move(regs));
        } else
            regMgr.loadFromFile(params.regions);

        if (!params.container.isSet()) {
            prnErr("No container has been set");
            return EXIT_FAILURE;
        }
        if (!params.codec.isSet()) {
            prnErr("No codec has been set");
            return EXIT_FAILURE;
        }
        if (params.output.isEmpty()) {
            prnErr("No output path");
            return EXIT_FAILURE;
        }
        if (params.posInf) {
            if (!checkPosInfParams(params))
                return EXIT_FAILURE;
        }

        if (params.container() == CmdContainers::SingleImages) {
            if (cmdCodecIsVideo(params.codec())) {
                prnErr("Single images container cannot be used with video codec");
                return EXIT_FAILURE;
            }

            prnInfo("Splitting video to images...");
            video.splitToImages(params.output, params.namingScheme, cmdCodecToImg(params.codec()), params.wellsToDirs, regMgr.regions());
            prnInfo("Splitting complete");
        } else {
            if (!cmdCodecIsVideo(params.codec())) {
                prnErr("Video container cannot be used with image codec");
                return EXIT_FAILURE;
            }

            const auto fps = params.fps.isSet() ? params.fps() : -1;

            prnInfo("Splitting video to videos...");
            video.splitToVideos(params.output, params.namingScheme, cmdCntToOpt(params.container()), cmdCodecToVid(params.codec()), fps, regMgr.regions());
            prnInfo("Splitting complete");
        }

        if (!params.posInf)
            return EXIT_SUCCESS;

        prnInfo("Inferring positions...");

        PositionInferenceRunner runner{
            params.output,
            params.posInfOutput,
            params.posInfScript,
            params.posInfModel,
            params.python3Path
        };

        if (params.wellsToDirs)
            return runner.subdirs() == QProcess::NormalExit ? EXIT_SUCCESS : EXIT_FAILURE;
        return runner.singleDir() == QProcess::NormalExit ? EXIT_SUCCESS : EXIT_FAILURE;
    } catch (const OpenCVVideo::Error &ex) {
        prnErr(ex.what());
        return EXIT_FAILURE;
    } catch (const RegionManager::Error &ex) {
        prnErr(ex.what());
        return EXIT_SUCCESS;
    }
}
