#ifndef REGIONMANAGER_H
#define REGIONMANAGER_H

#include <QException>
#include <QObject>
#include <QRect>
#include <QVector>

class RegionManager : public QObject {
    Q_OBJECT

public:
    class Error : public QException {
    public:
        Error(QString msg) :
            QException{},
            m_what{std::move(msg)}
        {}

        void raise() const { throw *this; }
        Error *clone() const { return new Error{*this}; }

        auto what() const noexcept -> const char *
        {
            return m_what.toLocal8Bit();
        }
    private:
        QString m_what;
    };

    RegionManager(QObject *parent = nullptr);
    auto clear() -> void;
    auto loadFromFile(const QString &path) -> void;
    auto provisionalRegion() const -> const QRect &;
    auto regions() const -> const QVector<QRect> &;
    auto saveToFile(QString path) -> void;
    auto setMaxSize(QSize maxSize) -> void;
    auto setRegions(QVector<QRect> newRegions) -> void;
    auto sort() -> void;
    auto updateRegion(const int idx, const QRect &newRegion) -> bool;

    static const QString FILE_SUFFIX;

private:
    QSize m_maxSize;
    QRect m_provisionalRegion;
    QVector<QRect> m_regions;

public slots:
    void addProvisionalRegion(const QRect &region);
    void addRegion(const QRect &region);
    void removeProvisionalRegion();
    void removeRegion(const QPoint &pos);
    void removeRegion(const int idx);

signals:
    void regionsChanged(const QVector<QRect> &regions, const QVector<int> changed);
    void provisionalRegionChanged(const QRect &region);
};

#endif // REGIONMANAGER_H
