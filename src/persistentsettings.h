#ifndef PERSISTENTSETTINGS_H
#define PERSISTENTSETTINGS_H

class QSettings;

class PersistentSettings {
public:
    PersistentSettings() = delete;

    static auto get() -> QSettings *;

private:
    static QSettings *s_settings;
};

#endif // PersistentSETTINGS_H
