#ifndef POSITIONINFERENCERUNNER_H
#define POSITIONINFERENCERUNNER_H

#include <QProcess>

class PositionInferenceRunner {
public:
    PositionInferenceRunner(QString imagesPath, QString outputPath, QString scriptPath, QString modelPath, QString python3Path);
    auto singleDir() -> QProcess::ExitStatus;
    auto subdirs() -> QProcess::ExitStatus;

private:
    QString m_imagesPath;
    QString m_modelPath;
    QString m_outputPath;
    QString m_python3Path;
    QString m_scriptPath;
};

#endif // POSITIONINFERENCERUNNER_H
