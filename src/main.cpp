#include <ui/mainwindow.h>

#include <cmdlineparser.h>
#include <globals.h>
#include <headlessrunner.h>

#include <QApplication>

#include <QDebug>

int main(int argc, char *argv[])
{
    QCoreApplication::setOrganizationName("FGU");
    QCoreApplication::setOrganizationDomain("cz.cas.fgu");
    QCoreApplication::setApplicationName("VideoSplitter");
    QCoreApplication::setApplicationVersion(Globals::versionString());

    QApplication a{argc, argv};

    auto userParams = CmdLineParser::process(a);

    if (userParams.headless)
        return runHeadless(userParams);
    else {
        MainWindow w{userParams};
        w.show();
        return a.exec();
    }
}
