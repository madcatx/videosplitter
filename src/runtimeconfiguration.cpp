#include "runtimeconfiguration.h"

RuntimeConfiguration * RuntimeConfiguration::s_me{nullptr};

RuntimeConfiguration::RuntimeConfiguration()
{
}

auto RuntimeConfiguration::get() -> RuntimeConfiguration *
{
    if (s_me == nullptr)
        s_me = new RuntimeConfiguration{};
    return s_me;
}
