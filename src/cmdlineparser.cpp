#include "cmdlineparser.h"

#include <globals.h>
#include <regionutil.h>

#include <QApplication>
#include <QLocale>

#include <cstdlib>
#include <iostream>

auto valueAsDouble(const QString &v, double &d)
{
    static const QLocale loc{QLocale::C};

    bool ok;
    d = loc.toDouble(v, &ok);
    return ok;
}

#define SET_DBL_OPT(p, opt, u) \
    if (p.isSet(opt)) { \
        double d; \
        if (!valueAsDouble(p.value(opt), d)) { \
            std::cerr << "Invalid value for " #opt << std::endl; \
            std::exit(EXIT_FAILURE); \
        } \
        u.opt.set(d); \
    }

#define SET_STR_OPT(p, opt, u) \
    if (p.isSet(opt)) \
        u.opt = p.value(opt)

template <size_t N>
auto _strArrToList(const char *arr[], QStringList &list)
{
    list << arr[0];
    return _strArrToList<N-1>(++arr, list);
}

template <>
auto _strArrToList<1>(const char *arr[], QStringList &list)
{
    list << arr[0];
    return list;
}

template <const char *A[], size_t N = sizeof(A) / sizeof(A[0])>
auto strArrToList()
{
    QStringList list{};
    return _strArrToList<N>(A, list).join(',');
}

static QCommandLineOption arfMaxAreaThr{
    "arfMaxAreaThr",
    QString{QObject::tr("Automatic region finder maximum area threshold. Allowed values: %1 - %2")}.arg(REGION_MIN_AREA).arg(REGION_MAX_AREA),
    "value"
};
static QCommandLineOption arfMinAreaThr{
    "arfMinAreaThr",
    QString{QObject::tr("Automatic region finder minimum area threshold. Allowed values: %1 - %2")}.arg(REGION_MIN_AREA).arg(REGION_MAX_AREA),
    "value"
};
static QCommandLineOption arfScaleFactor{
    "arfScaleFactor",
    QString{QObject::tr("Automatic region finder scale factor. Allowed values: %1 - %2")}.arg(REGION_MIN_SCALE_FACTOR).arg(REGION_MAX_SCALE_FACTOR),
    "value"
};
static QCommandLineOption codec{
    "codec",
    QString{QObject::tr("Output codec.\nAvailable options: %1")}.arg(strArrToList<CmdCodecsNames>()),
    "codec"
};
static QCommandLineOption container{
    "container",
    QString{QObject::tr("Output container. Available options: %1")}.arg(strArrToList<CmdContainersNames>()),
    "container"
};
static QCommandLineOption fps{
    "fps",
    QObject::tr("Framerate of the input video. Autodetected if not specified"),
    "value"
};
static QCommandLineOption headless{"headless", QObject::tr("Run the application in headless mode")};
static QCommandLineOption input{"input", QObject::tr("Input video file"), "file"};
static QCommandLineOption namingScheme{
    "namingScheme",
    QObject::tr("Naming scheme. Automatic naming scheme is used if this option is not specified"),
    "scheme"
};

static QCommandLineOption posInf{
    "posInf",
    QObject::tr("Run position inference script after splitting")
};
static QCommandLineOption posInfModel{
    "posInfModel",
    QObject::tr("Path to positon inference neural net model"),
    "path"
};
static QCommandLineOption posInfOutput{
    "posInfOutput",
    QObject::tr("Path where to save position inference results. If this parameter is not specified, output will be saved to the source data directory"),
    "path"
};
static QCommandLineOption posInfScript{
    "posInfScript",
    QObject::tr("Path to position inference script"),
    "path"
};
static QCommandLineOption python3Path{
    "python3Path",
    QObject::tr("Path to Python 3 installation. The application will try to use system Python 3 if the parameter is not specified"),
    "path"
};
static QCommandLineOption output{"output", QObject::tr("Output path"), "path"};
static QCommandLineOption regions{"regions", QObject::tr("Regions file. Regions will be detected automatically if the parameter is not specified"), "file"};
static QCommandLineOption wellsToDirs{
    "wellsToDirs",
    QObject::tr("Write images for each well into a separate directory. This option is useful only with the \"images\" container")
};

CmdLineParser * CmdLineParser::s_me{nullptr};

CmdLineParser::CmdLineParser()
{
    m_parser.setApplicationDescription(Globals::APPLICATION_NAME);
    m_parser.addHelpOption();
    m_parser.addVersionOption();

    m_parser.addOption(arfMaxAreaThr);
    m_parser.addOption(arfMinAreaThr);
    m_parser.addOption(arfScaleFactor);
    m_parser.addOption(codec);
    m_parser.addOption(container);
    m_parser.addOption(headless);
    m_parser.addOption(fps);
    m_parser.addOption(input);
    m_parser.addOption(namingScheme);
    m_parser.addOption(output);
    m_parser.addOption(regions);
    m_parser.addOption(wellsToDirs);
    m_parser.addOption(posInf);
    m_parser.addOption(posInfModel);
    m_parser.addOption(posInfOutput);
    m_parser.addOption(posInfScript);
    m_parser.addOption(python3Path);
}

auto CmdLineParser::process(QApplication &app) -> CmdUserParams
{
    if (s_me == nullptr)
        s_me = new CmdLineParser{};

    auto &p = s_me->m_parser;
    p.process(app);

    CmdUserParams params{};

    SET_DBL_OPT(p, arfMaxAreaThr, params);
    SET_DBL_OPT(p, arfMinAreaThr, params);
    SET_DBL_OPT(p, arfScaleFactor, params);

    if (p.isSet(codec)) {
        auto v = p.value(codec);
        try {
            params.codec = stringToCmdCodec(v);
        }  catch (const std::runtime_error &) {
            std::cout << "WARNING: Codec value " << v.toStdString() << "is invalid" << std::endl;
        }
    }
    if (p.isSet(container)) {
        auto v = p.value(container);
        try {
            params.container = stringToCmdContainer(v);
        }  catch (const std::runtime_error &) {
            std::cout << "WARNING: Container value " << v.toStdString() << "is invalid" << std::endl;
        }
    }

    params.headless = p.isSet(headless);
    SET_STR_OPT(p, input, params);
    SET_STR_OPT(p, namingScheme, params);
    SET_STR_OPT(p, output, params);
    SET_STR_OPT(p, regions, params);
    params.wellsToDirs = p.isSet(wellsToDirs);
    SET_DBL_OPT(p, fps, params);

    params.posInf = p.isSet(posInf);
    SET_STR_OPT(p, posInfModel, params);
    SET_STR_OPT(p, posInfOutput, params);
    SET_STR_OPT(p, posInfScript, params);
    SET_STR_OPT(p, python3Path, params);

    return params;
}
