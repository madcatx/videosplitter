#ifndef REGIONUTIL_H
#define REGIONUTIL_H

#include <opencv2/core.hpp>

#include <QRect>
#include <QVector>

#define REGION_MAX_AREA 100.0
#define REGION_MIN_AREA 0.0
#define REGION_MAX_SCALE_FACTOR 100.0
#define REGION_MIN_SCALE_FACTOR -100.0

template <typename T>
auto within(const T &min, const T &max, const T &val)
{
    return val < max && val > min;
}

template <typename T>
auto clamp(const T &min, const T &max, T &val, const T &def)
{
    if (!within(min, max, val))
        val = def;
}

namespace RegionUtil {

auto find(const cv::Mat &src, const double minAreaThreshold, const double maxAreaThreshold, const double scaleFactor) -> QVector<QRect>;
auto sort(const QRect &first, const QRect &second) -> bool;

} // namespace RegionUtil

#endif // REGIONUTIL_H
