#include "globals.h"

const QString Globals::APPLICATION_NAME{"VideoSplitter"};
const int Globals::VER_MAJ{0};
const int Globals::VER_MIN{0};
const char Globals::VER_REV{'a'};

auto Globals::versionString() -> QString
{
    return QString{"%1.%2%3"}.arg(VER_MAJ).arg(VER_MIN).arg(VER_REV);
}
