#include <cmduserparams.h>

#include <cassert>

_X_MK_ENUM_NAMES_DEF(CmdContainers, AVI, MP4, MKV, SingleImages);
_X_MK_ENUM_NAMES_DEF(CmdCodecs, JPEG, PNG, MJPEG, XVID, H264, H265);

#define CMP_CODEC(str, codec) \
    if (str.compare(#codec) == 0) return CmdCodecs::codec

#define CMP_CONTAINER(str, cnt) \
    if (str.compare(#cnt, Qt::CaseInsensitive) == 0) return CmdContainers::cnt

auto cmdCodecIsVideo(const CmdCodecs c) -> bool
{
    switch (c) {
    case CmdCodecs::JPEG:
    case CmdCodecs::PNG:
        return false;
    case CmdCodecs::MJPEG:
    case CmdCodecs::XVID:
    case CmdCodecs::H264:
    case CmdCodecs::H265:
        return true;
    default:
        assert(false);
    }
}

auto stringToCmdCodec(const QString &str) -> CmdCodecs
{
    const auto upr = str.toUpper();
    CMP_CODEC(upr, JPEG);
    CMP_CODEC(upr, PNG);
    CMP_CODEC(upr, MJPEG);
    CMP_CODEC(upr, XVID);
    CMP_CODEC(upr, H264);
    CMP_CODEC(upr, H265);

    throw std::runtime_error{"Unknown codec"};
}

auto stringToCmdContainer(const QString &str) -> CmdContainers
{
    CMP_CONTAINER(str, SingleImages);
    CMP_CONTAINER(str, AVI);
    CMP_CONTAINER(str, MP4);
    CMP_CONTAINER(str, MKV);

    throw std::runtime_error{"Unknown container"};
}
