#ifndef REGIONTABLEMODEL_H
#define REGIONTABLEMODEL_H

#include <QAbstractTableModel>

class RegionManager;

class RegionTableModel : public QAbstractTableModel {
    Q_OBJECT

public:
    RegionTableModel(RegionManager &mgr, QObject *parent = nullptr);
    auto columnCount(const QModelIndex &parent = {}) const -> int override;
    auto data(const QModelIndex &idx, int role) const -> QVariant override;
    auto flags(const QModelIndex &idx) const -> Qt::ItemFlags override;
    auto headerData(int section, Qt::Orientation orientation, int role) const -> QVariant override;
    auto rowCount(const QModelIndex &parent = {}) const -> int override;
    auto setData(const QModelIndex &idx, const QVariant &value, int role) -> bool override;

private:
    RegionManager &h_regionMgr;
    int m_lastRegionCount;

private slots:
    void onRegionsChanged(const QVector<QRect> &regions, const QVector<int> &changed);
};

#endif // REGIONTABLEMODEL_H
