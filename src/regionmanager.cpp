#include "regionmanager.h"

#include <regionutil.h>

#include <QFile>
#include <QLocale>
#include <QTextStream>

static const QChar DELIM{';'};

const QString RegionManager::FILE_SUFFIX{"csv"};

inline
auto normAndClamp(const QRect &reg, const QSize &maxSize)
{
    auto fixed = reg.normalized();
    if (fixed.left() < 0)
        fixed.setLeft(0);
    if (fixed.top() < 0)
        fixed.setTop(0);
    if (fixed.right() >= maxSize.width())
        fixed.setRight(maxSize.width() - 1);
    if (fixed.bottom() >= maxSize.height())
        fixed.setBottom(maxSize.height() - 1);

    return fixed;
}

RegionManager::RegionManager(QObject *parent) :
    QObject{parent}
{
}

void RegionManager::addProvisionalRegion(const QRect &region)
{
    m_provisionalRegion = normAndClamp(region, m_maxSize);
    emit provisionalRegionChanged(m_provisionalRegion);
}

void RegionManager::addRegion(const QRect &region)
{
    m_regions.push_back(normAndClamp(region, m_maxSize));
    emit regionsChanged(m_regions, { m_regions.size() - 1 });
}

auto RegionManager::clear() -> void
{
    m_regions.clear();
    emit regionsChanged(m_regions, {});
}

auto RegionManager::loadFromFile(const QString &path) -> void
{
    QFile fh{path};
    if (!fh.open(QIODevice::ReadOnly | QIODevice::Text))
        throw Error{"Failed to open file"};

    QTextStream stm{&fh};
    stm.setCodec("UTF-8");
    stm.setLocale({QLocale::English, QLocale::UnitedStates});

    QVector<QRect> m_newRegions{};

    int ctr{1};
    while (!stm.atEnd()) {
        const auto line = stm.readLine();
        const auto segments = line.split(DELIM);
        if (segments.size() != 4)
            throw Error{QString{"Invalid number of columns on line %1"}.arg(ctr)};

        QRect reg{};
        auto setPos = [&reg, ctr](const QString &data, void (QRect::*setter)(int)) {
            bool ok;
            const auto v = data.toInt(&ok);
            if (!ok)
                throw Error{QString{"Non-numeric data on line %1"}.arg(ctr)};
            (reg.*setter)(v);
        };
        setPos(segments[0], &QRect::setLeft);
        setPos(segments[1], &QRect::setTop);
        setPos(segments[2], &QRect::setRight);
        setPos(segments[3], &QRect::setBottom);

        m_newRegions.push_back(reg);
        ctr++;
    }

    m_regions = std::move(m_newRegions);
    emit regionsChanged(m_regions, {});
}

auto RegionManager::provisionalRegion() const -> const QRect &
{
    return m_provisionalRegion;
}

auto RegionManager::regions() const -> const QVector<QRect> &
{
    return m_regions;
}

void RegionManager::removeProvisionalRegion()
{
    m_provisionalRegion = {};
    emit provisionalRegionChanged(m_provisionalRegion);
}

void RegionManager::removeRegion(const QPoint &pos)
{
    for (auto idx = 0; idx < m_regions.size(); idx++) {
        const auto &region = m_regions[idx];
        if (region.contains(pos)) {
            m_regions.removeAt(idx);
            emit regionsChanged(m_regions, { -idx });
            return;
        }
    }
}

void RegionManager::removeRegion(const int idx)
{
    if (idx < 0 || idx >= m_regions.size())
        return;

    m_regions.removeAt(idx);
    emit regionsChanged(m_regions, { -idx });
}

auto RegionManager::saveToFile(QString path) -> void
{
    if (!path.endsWith(FILE_SUFFIX, Qt::CaseInsensitive))
        path += "." + FILE_SUFFIX;

    QFile fh{path};
    if (!fh.open(QIODevice::WriteOnly | QIODevice::Text))
        throw Error{"Cannot open output file"};

    QTextStream stm{&fh};
    stm.setCodec("UTF-8");

    auto str = [](const int n) {
        return QString::number(n);
    };
    for (const auto &reg : m_regions)
        stm << str(reg.left()) << DELIM << str(reg.top()) << DELIM << str(reg.right()) << DELIM << str(reg.bottom()) << "\n";
}

auto RegionManager::setMaxSize(QSize maxSize) -> void
{
    m_maxSize = maxSize;

    for (auto idx = 0; idx < m_regions.size(); idx++)
        m_regions[idx] = normAndClamp(m_regions[idx], m_maxSize);
}

auto RegionManager::setRegions(QVector<QRect> regions) -> void
{
    for (auto idx = 0; idx < regions.size(); idx++)
        regions[idx] = normAndClamp(regions[idx], m_maxSize);
    m_regions = std::move(regions);
    emit regionsChanged(m_regions, {});
}

auto RegionManager::sort() -> void
{
    std::sort(m_regions.begin(), m_regions.end(), RegionUtil::sort);
    emit regionsChanged(m_regions, {});
}

auto RegionManager::updateRegion(const int idx, const QRect &newRegion) -> bool
{
    if (idx < 0 || idx >= m_regions.size())
        return false;
    m_regions[idx] = normAndClamp(newRegion, m_maxSize);
    emit regionsChanged(m_regions, { idx });
    return true;
}
