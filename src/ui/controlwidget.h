#ifndef CONTROLWIDGET_H
#define CONTROLWIDGET_H

#include <outputoptions.h>

#include <QWidget>

class CmdUserParams;
class QFileDialog;
class RegionManager;

namespace Ui {
class ControlWidget;
}

class ControlWidget : public QWidget {
    Q_OBJECT

public:
    explicit ControlWidget(const CmdUserParams &userParams, RegionManager &mgr, QWidget *parent = nullptr);
    ~ControlWidget();
    auto outputPath() const -> QString;
    auto wellsToDirs() const -> bool;

private:
    auto applyUserParams(const CmdUserParams &params) -> void;
    template <typename T>
    auto currentCodec() -> T;

    template <typename T>
    auto selectCodec(const T codec) -> void;

    auto selectContainer(const OutputOptions::Containers cnt) -> void;
    auto setOutputMode() -> void;

    Ui::ControlWidget *ui;

    QFileDialog *m_loadVideoDlg;
    QFileDialog *m_saveOutputDlg;

    RegionManager &h_regionMgr;

private slots:
    void onBrowseOutputClicked();
    void onBrowseVideoClicked();
    void onSplitVideoClicked();

signals:
    void findRegions();
    void loadVideo(const QString &path);
    void splitVideo(const OutputOptions::Config &config);
};

#endif // CONTROLWIDGET_H
