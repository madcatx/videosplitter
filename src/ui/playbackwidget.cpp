#include "playbackwidget.h"
#include "ui_playbackwidget.h"

#include <regionmanager.h>

#include <QMessageBox>

PlaybackWidget::PlaybackWidget(RegionManager &mgr, QWidget *parent) :
    QWidget{parent},
    ui{new Ui::PlaybackWidget},
    h_regionMgr{mgr}
{
    ui->setupUi(this);

    clearFramesCounter();

    connect(ui->qpb_firstFrame, &QPushButton::clicked,
            [this]() {
                showFrame(0);
            });
    connect(ui->qpb_prevFrame, &QPushButton::clicked,
            [this]() {
                if (m_currentFrameNum > 0)
                    showFrame(m_currentFrameNum - 1);
            });
    connect(ui->qpb_nextFrame, &QPushButton::clicked,
            [this]() {
                if (m_video == nullptr)
                    return;
                if (m_currentFrameNum < m_video->numFrames() - 1)
                    showFrame(m_currentFrameNum + 1);
            });
    connect(ui->qpb_lastFrame, &QPushButton::clicked,
            [this]() {
                if (m_video != nullptr)
                    showFrame(m_video->numFrames() - 1);
            });

    connect(&h_regionMgr, &RegionManager::regionsChanged, this, &PlaybackWidget::drawCurrentFrame);
    connect(&h_regionMgr, &RegionManager::provisionalRegionChanged, this, &PlaybackWidget::drawCurrentFrame);
}

PlaybackWidget::~PlaybackWidget()
{
    delete ui;
}

auto PlaybackWidget::clearFramesCounter() -> void
{
    ui->ql_frameNum->setText("-/-");
}

bool PlaybackWidget::drawCurrentFrame()
{
    const auto &image = m_currentFrame.image(h_regionMgr);
    if (image.isNull())
        return false;

    QPixmap pixmap{};
    if (!pixmap.convertFromImage(image)) {
        QMessageBox::warning(this,
                             "Image error",
                             "Cannot convert image data to pixmap");
        return false;
    }

    if (pixmap.isNull())
        return false;

    ui->ql_frame->setPicture(std::move(pixmap));
    return true;
}

auto PlaybackWidget::currentFrameNum() const -> uint_fast64_t
{
    return m_currentFrameNum;
}

auto PlaybackWidget::setFramesCounter(const uint_fast64_t num) -> void
{
    if (m_video == nullptr)
        clearFramesCounter();
    ui->ql_frameNum->setText(QString{"%1/%2"}.arg(num).arg(m_video->numFrames()));
}

auto PlaybackWidget::setVideo(std::shared_ptr<OpenCVVideo> video) -> void
{
    m_video = video;

    if (m_video != nullptr) {
        connect(ui->ql_frame, &VideoFrameLabel::regionSelected, &h_regionMgr, &RegionManager::addRegion);
        connect(ui->ql_frame, &VideoFrameLabel::removeRegion, &h_regionMgr, static_cast<void (RegionManager::*)(const QPoint &)>(&RegionManager::removeRegion));
        connect(ui->ql_frame, &VideoFrameLabel::regionMarked, &h_regionMgr, &RegionManager::addProvisionalRegion);
        connect(ui->ql_frame, &VideoFrameLabel::regionUnmarked, &h_regionMgr, &RegionManager::removeProvisionalRegion);
        showFrame(0);
    } else {
        disconnect(ui->ql_frame, &VideoFrameLabel::regionSelected, &h_regionMgr, &RegionManager::addRegion);
        disconnect(ui->ql_frame, &VideoFrameLabel::removeRegion, &h_regionMgr, static_cast<void (RegionManager::*)(const QPoint &)>(&RegionManager::removeRegion));
        disconnect(ui->ql_frame, &VideoFrameLabel::regionMarked, &h_regionMgr, &RegionManager::addProvisionalRegion);
        disconnect(ui->ql_frame, &VideoFrameLabel::regionUnmarked, &h_regionMgr, &RegionManager::removeProvisionalRegion);
    }
}

auto PlaybackWidget::showFrame(const uint_fast64_t num) -> void
{
    if (m_video != nullptr && !m_video->isValid())
        return;

    m_currentFrame = m_video->frame(num);
    if (drawCurrentFrame()) {
        m_currentFrameNum = num;
        setFramesCounter(num);
    }
}
