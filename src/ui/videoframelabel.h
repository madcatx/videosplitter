#ifndef VIDEOFRAMELABEL_H
#define VIDEOFRAMELABEL_H

#include <QLabel>
#include <QPixmap>

class VideoFrameLabel : public QLabel {
    Q_OBJECT

public:
    VideoFrameLabel(QWidget *parent = nullptr);
    auto setPicture(QPixmap pixmap) -> void;

protected:
    auto mouseMoveEvent(QMouseEvent *ev) -> void override;
    auto mousePressEvent(QMouseEvent *ev) -> void override;
    auto mouseReleaseEvent(QMouseEvent *ev) -> void override;
    auto resizeEvent(QResizeEvent *ev) -> void override;

private:
    auto scaleToSource(const QPoint &pt) -> QPoint;
    auto scaleToSource(const QRect &rect) -> QRect;

    QPixmap m_picture;
    bool m_selectionOn;
    QPoint m_selectionFrom;

signals:
    void regionMarked(const QRect &region);
    void regionSelected(const QRect &region);
    void regionUnmarked();
    void removeRegion(const QPoint &pos);
};

#endif // VIDEOFRAMELABEL_H
