#ifndef PLAYBACKWIDGET_H
#define PLAYBACKWIDGET_H

#include <opencvvideo.h>

#include <QWidget>

#include <cstdint>
#include <memory>

class RegionManager;

namespace Ui {
class PlaybackWidget;
}

class PlaybackWidget : public QWidget {
    Q_OBJECT

public:
    explicit PlaybackWidget(RegionManager &mgr, QWidget *parent = nullptr);
    ~PlaybackWidget();
    auto currentFrameNum() const -> uint_fast64_t;
    auto setVideo(std::shared_ptr<OpenCVVideo> video) -> void;

private:
    auto clearFramesCounter() -> void;
    auto setFramesCounter(const uint_fast64_t num) -> void;
    auto showFrame(const uint_fast64_t num) -> void;

    Ui::PlaybackWidget *ui;

    std::shared_ptr<OpenCVVideo> m_video;
    VideoFrame m_currentFrame;
    uint_fast64_t m_currentFrameNum;

    RegionManager &h_regionMgr;

private slots:
    bool drawCurrentFrame();
};

#endif // PLAYBACKWIDGET_H
