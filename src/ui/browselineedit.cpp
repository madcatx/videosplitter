#include "browselineedit.h"
#include "ui_browselineedit.h"

#include <QFileInfo>

BrowseLineEdit::BrowseLineEdit(QWidget *parent) :
    QWidget{parent},
    ui{new Ui::BrowseLineEdit}
{
    ui->setupUi(this);

    m_dlg = new QFileDialog{this};

    connect(ui->qpb_browse, &QPushButton::clicked, [this]() {
        QFileInfo finfo{ui->qle_path->text()};
        if (finfo.absoluteDir().exists())
            m_dlg->setDirectory(finfo.isDir() ? finfo.absoluteFilePath() : finfo.absolutePath());
        if (m_dlg->exec() == QDialog::Accepted)
            ui->qle_path->setText(m_dlg->selectedFiles().constFirst());
    });
}

BrowseLineEdit::~BrowseLineEdit()
{
    delete ui;
}

auto BrowseLineEdit::path() const -> QString
{
    return ui->qle_path->text();
}

auto BrowseLineEdit::setAcceptMode(const QFileDialog::AcceptMode mode) -> void
{
    m_dlg->setAcceptMode(mode);
}

auto BrowseLineEdit::setFileMode(const QFileDialog::FileMode mode) -> void
{
    m_dlg->setFileMode(mode);
}

auto BrowseLineEdit::setNameFilters(const QStringList &filters) -> void
{
    m_dlg->setNameFilters(filters);
}

auto BrowseLineEdit::setOption(const QFileDialog::Option option, const bool on) -> void
{
#ifdef USE_QT_DIALOG
    if (option == QFileDialog::DontUseNativeDialog)
        return;
#else
    m_dlg->setOption(option, on);
#endif // USE_QT_DIALOG
}

auto BrowseLineEdit::setOptions(const QFileDialog::Options options) -> void
{
#ifdef USE_QT_DIALOG
    m_dlg->setOptions(options | QFileDialog::DontUseNativeDialog);
#else
    m_dlg->setOptions(options);
#endif // USE_QT_DIALOG
}

auto BrowseLineEdit::setPath(const QString &path) -> void
{
    ui->qle_path->setText(path);
}
