#ifndef CONFIGURATIONDIALOG_H
#define CONFIGURATIONDIALOG_H

#include <QDialog>

namespace Ui {
class ConfigurationDialog;
}

class CmdUserParams;

class ConfigurationDialog : public QDialog {
    Q_OBJECT

public:
    explicit ConfigurationDialog(const CmdUserParams &userParams, QWidget *parent = nullptr);
    ~ConfigurationDialog();

private:
    auto applyUserParams(const CmdUserParams &params) -> void;
    auto fillFromConfiguration() -> void;

    Ui::ConfigurationDialog *ui;

private slots:
    void onAccepted();
    void onRejected();
};

#endif // CONFIGURATIONDIALOG_H
