#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <cmduserparams.h>
#include <globals.h>
#include <opencvvideo.h>
#include <regionutil.h>
#include <ui/configurationdialog.h>
#include <ui/controlwidget.h>
#include <ui/inprogressdialog.h>
#include <ui/regionfinderoptionsdialog.h>
#include <ui/runinferpositionsdialog.h>

#include <QApplication>
#include <QEventLoop>
#include <QFileDialog>
#include <QMessageBox>
#include <QSplitter>
#include <QtConcurrent/QtConcurrent>

#include <type_traits>

MainWindow::MainWindow(const CmdUserParams &userParams, QWidget *parent) :
    QMainWindow{parent},
    ui{new Ui::MainWindow}
{
    ui->setupUi(this);

    auto splitter = new QSplitter{Qt::Horizontal, this};
    m_playback = new PlaybackWidget{m_regionMgr, this};
    auto control = new ControlWidget{userParams, m_regionMgr, this};
    m_regFindOptsDlg = new RegionFinderOptionsDialog{userParams, this};
    m_runInferPosDlg = new RunInferPositionsDialog{userParams, this};
    auto cfgDlg = new ConfigurationDialog{userParams, this};

    splitter->addWidget(m_playback);
    splitter->addWidget(control);

    this->setCentralWidget(splitter);

    m_loadSaveRegionsDlg = new QFileDialog{this,
                                           "",
                                           qApp->applicationDirPath()};
#ifdef USE_QT_DIALOGS
    m_loadSaveRegionsDlg->setOption(QFileDialog::DontUseNativeDialog, true);
#endif // USE_QT_DIALOGS
    m_loadSaveRegionsDlg->setNameFilters({QString{"Regions files (*.%1)"}.arg(RegionManager::FILE_SUFFIX), "All files(*.*)"});

    connect(control, &ControlWidget::loadVideo, this, &MainWindow::onLoadVideo);
    connect(ui->actionNew_regions, &QAction::triggered,
            [this]() {
                m_regionMgr.clear();
                setCurrentRegions("");
            });
    connect(control, &ControlWidget::splitVideo, this, &MainWindow::onSplitVideo);
    connect(ui->actionLoad_regions, &QAction::triggered, this, &MainWindow::onLoadRegions);
    connect(ui->actionSave_regions, &QAction::triggered, this, &MainWindow::onSaveRegions);
    connect(ui->actionSave_regions_as, &QAction::triggered, this, &MainWindow::onSaveRegionsAs);
    connect(ui->actionFinder_options, &QAction::triggered, [this]() { m_regFindOptsDlg->exec(); });
    connect(control, &ControlWidget::findRegions, this, &MainWindow::onFindRegions);
    connect(ui->actionInfer_positions, &QAction::triggered,
            [this, control]() {
                m_runInferPosDlg->setImagesPath(control->outputPath());
                m_runInferPosDlg->setImagesInSubdirs(control->wellsToDirs());
                m_runInferPosDlg->exec();
            });
    connect(ui->actionConfiguration, &QAction::triggered, [cfgDlg]() { cfgDlg->exec(); });

    setTitle();

    applyUserParams(userParams);
}

MainWindow::~MainWindow()
{
    delete ui;
}

auto MainWindow::applyUserParams(const CmdUserParams &params) -> void
{
    if (!params.input.isEmpty())
        onLoadVideo(params.input);
    if (!params.regions.isEmpty())
        loadRegions(params.regions);
}

auto MainWindow::loadRegions(const QString &path) -> void
{
    try {
        m_regionMgr.loadFromFile(path);
        setCurrentRegions(path);
    } catch (const RegionManager::Error &ex) {
        QMessageBox::warning(this, "I/O error", ex.what());
    }
}

void MainWindow::onFindRegions()
{
    if (m_video == nullptr)
        return;

    auto frame = m_video->frameRaw(m_playback->currentFrameNum());
    if (frame.empty())
        return;

    auto regions = RegionUtil::find(frame, m_regFindOptsDlg->minAreaThreshold(), m_regFindOptsDlg->maxAreaThreshold(), m_regFindOptsDlg->scaleFactor());
    if (regions.empty()) {
        QMessageBox::information(this, "No data", "No regions were indentified on the current frame");
        return;
    }

    m_regionMgr.setRegions(regions);
}

void MainWindow::onLoadRegions()
{
    if (m_video == nullptr) {
        QMessageBox::information(this, "Information", "Please load a video before loading regions");
        return;
    }

    m_loadSaveRegionsDlg->setWindowTitle("Load regions");
    m_loadSaveRegionsDlg->setAcceptMode(QFileDialog::AcceptOpen);
    if (!m_currentRegionsPath.isEmpty())
        m_loadSaveRegionsDlg->setDirectory(m_currentRegionsPath);

    if (m_loadSaveRegionsDlg->exec() == QDialog::Accepted) {
        const auto sel = m_loadSaveRegionsDlg->selectedFiles().constFirst();
        loadRegions(sel);
    }
}

void MainWindow::onLoadVideo(const QString &path)
{
    if (path.isEmpty())
        return;

    if (!QFileInfo{path}.exists()) {
        QMessageBox::warning(this,
                             "Invalid input",
                             "No such video file");
        return;
    }

    try {
        m_video = std::make_shared<OpenCVVideo>(path);
        m_regionMgr.setMaxSize(m_video->frameSize());
        m_playback->setVideo(m_video);
    } catch (const OpenCVVideo::Error &ex) {
        QMessageBox::warning(this,
                             "Video error",
                             ex.what());
    }
}

void MainWindow::onSaveRegions()
{
    if (!m_currentRegionsPath.isEmpty()) {
        try {
            m_regionMgr.saveToFile(m_currentRegionsPath);
        } catch (const RegionManager::Error &ex) {
            QMessageBox::warning(this, "I/O error", ex.what());
        }
    } else
        onSaveRegionsAs();
}

void MainWindow::onSaveRegionsAs()
{
    if (m_video == nullptr)
        return;

    if (m_regionMgr.regions().empty()) {
        QMessageBox::information(this, "Information", "Regions are empty");
        return;
    }

    m_loadSaveRegionsDlg->setAcceptMode(QFileDialog::AcceptSave);
    m_loadSaveRegionsDlg->setWindowTitle("Save regions as");
    if (!m_currentRegionsPath.isEmpty())
        m_loadSaveRegionsDlg->setDirectory(m_currentRegionsPath);
    if (m_loadSaveRegionsDlg->exec() == QDialog::Accepted) {
        try {
            const auto sel = m_loadSaveRegionsDlg->selectedFiles().constFirst();
            m_regionMgr.saveToFile(sel);
            setCurrentRegions(sel);
        } catch (const RegionManager::Error &ex) {
            QMessageBox::warning(this, "I/O error", ex.what());
        }
    }
}

void MainWindow::onSplitVideo(const OutputOptions::Config &config)
{
    if (m_video == nullptr)
        return;

    QEventLoop loop{};
    InProgressDialog dlg{"Splitting video", this};
    dlg.setModal(true);

    auto fun = [this, config]() {
        if (config.container == OutputOptions::SingleImages)
            m_video->splitToImages(config.path, config.prefix, config.imageCodec, config.wellsToDirs, m_regionMgr.regions());
        else
            m_video->splitToVideos(config.path, config.prefix, config.container, config.videoCodec, config.fps, m_regionMgr.regions());
    };
    using FutureType = std::result_of_t<decltype (fun)()>;

    QFutureWatcher<FutureType> watcher{};

    connect(&watcher, &QFutureWatcher<FutureType>::started, &dlg, &InProgressDialog::show);
    connect(&watcher, &QFutureWatcher<FutureType>::finished, &dlg, &InProgressDialog::accept);
    connect(&watcher, &QFutureWatcher<FutureType>::finished, &loop, &QEventLoop::quit);

    auto fut = QtConcurrent::run(fun);
    watcher.setFuture(fut);

    loop.exec();

    try {
        fut.waitForFinished();
    } catch (const OpenCVVideo::Error &ex) {
        QMessageBox::warning(this,
                             "Processing error",
                             QString{"An error occured during splitting:\n%1"}.arg(ex.what()));
    }
}

auto MainWindow::setCurrentRegions(const QString &path) -> void
{
    m_currentRegionsPath = path;
    setTitle();
}

auto MainWindow::setTitle() -> void
{
    if (m_currentRegionsPath.isEmpty())
        setWindowTitle(Globals::APPLICATION_NAME);
    else
        setWindowTitle(QString{"%1 - %2"}.arg(Globals::APPLICATION_NAME, m_currentRegionsPath));
}
