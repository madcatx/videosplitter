#include "regionfinderoptionsdialog.h"
#include "ui_regionfinderoptionsdialog.h"

#include <cmduserparams.h>
#include <regionutil.h>
#include <runtimeconfiguration.h>

#include <cassert>

#define DEFAULT_MAX_AREA_THRESHOLD 10.0
#define DEFAULT_MIN_AREA_THRESHOLD 2.5
#define DEFAULT_SCALE_FACTOR 3.0

RegionFinderOptionsDialog::RegionFinderOptionsDialog(const CmdUserParams &userParams, QWidget *parent) :
    QDialog{parent},
    ui{new Ui::RegionFinderOptionsDialog}
{
    ui->setupUi(this);

    auto cfg = RuntimeConfiguration::get();

    m_maxAreaThreshold = cfg->finderMaxAreaThreshold();
    clamp(REGION_MIN_AREA, REGION_MAX_AREA, m_maxAreaThreshold, DEFAULT_MAX_AREA_THRESHOLD);

    m_minAreaThreshold = cfg->finderMinAreaThreshold();
    clamp(REGION_MIN_AREA, REGION_MAX_AREA, m_minAreaThreshold, DEFAULT_MIN_AREA_THRESHOLD);

    m_scaleFactor = cfg->finderScaleFactor();
    clamp(REGION_MIN_SCALE_FACTOR, REGION_MAX_SCALE_FACTOR, m_scaleFactor, DEFAULT_SCALE_FACTOR);

    ui->qdspb_maxAreaThreshold->setValue(m_maxAreaThreshold);
    ui->qdspb_minAreaThreshold->setValue(m_minAreaThreshold);
    ui->qdspb_scaleFactor->setValue(m_scaleFactor);

    ui->qdspb_maxAreaThreshold->setMinimum(m_minAreaThreshold + 0.5);
    ui->qdspb_minAreaThreshold->setMaximum(m_maxAreaThreshold - 0.5);

    connect(ui->qdspb_minAreaThreshold, static_cast<void (QDoubleSpinBox::*)(double)>(&QDoubleSpinBox::valueChanged), [this](const double v) {
        ui->qdspb_maxAreaThreshold->setMinimum(v + 0.5);
    });
    connect(ui->qdspb_maxAreaThreshold, static_cast<void (QDoubleSpinBox::*)(double)>(&QDoubleSpinBox::valueChanged), [this](const double v) {
        ui->qdspb_minAreaThreshold->setMaximum(v - 0.5);
    });

    connect(ui->buttonBox, &QDialogButtonBox::rejected, [this]() {
        ui->qdspb_maxAreaThreshold->setValue(m_maxAreaThreshold);
        ui->qdspb_minAreaThreshold->setValue(m_minAreaThreshold);
        ui->qdspb_scaleFactor->setValue(m_scaleFactor);
        reject();
    });
    connect(ui->buttonBox, &QDialogButtonBox::accepted, [this, cfg]() {
        m_maxAreaThreshold = ui->qdspb_maxAreaThreshold->value();
        assert(within(0.0, 100.0, m_maxAreaThreshold));

        m_minAreaThreshold = ui->qdspb_minAreaThreshold->value();
        assert(within(0.0, 100.0, m_minAreaThreshold));

        m_scaleFactor = ui->qdspb_scaleFactor->value();
        assert(within(-100.0, 100.0, m_scaleFactor));

        cfg->set_finderMaxAreaThreshold(m_maxAreaThreshold);
        cfg->set_finderMinAreaThreshold(m_minAreaThreshold);
        cfg->set_finderScaleFactor(m_scaleFactor);
        accept();
    });

    applyUserParams(userParams);
}

RegionFinderOptionsDialog::~RegionFinderOptionsDialog()
{
    delete ui;
}

auto RegionFinderOptionsDialog::applyUserParams(const CmdUserParams &params) -> void
{
    if (params.arfMinAreaThr.isSet()) {
        ui->qdspb_minAreaThreshold->setValue(params.arfMinAreaThr());
        m_minAreaThreshold = ui->qdspb_minAreaThreshold->value();
        ui->qdspb_maxAreaThreshold->setMinimum(m_minAreaThreshold + 0.5);
    }

    if (params.arfMaxAreaThr.isSet()) {
        ui->qdspb_maxAreaThreshold->setValue(params.arfMaxAreaThr());
        m_maxAreaThreshold = ui->qdspb_maxAreaThreshold->value();
        ui->qdspb_minAreaThreshold->setMaximum(m_maxAreaThreshold - 0.5);
    }

    if (params.arfScaleFactor.isSet()) {
        ui->qdspb_scaleFactor->setValue(params.arfScaleFactor());
        m_scaleFactor = ui->qdspb_scaleFactor->value();
    }
}

auto RegionFinderOptionsDialog::maxAreaThreshold() const -> double
{
    return m_maxAreaThreshold / 100.0;
}

auto RegionFinderOptionsDialog::minAreaThreshold() const -> double
{
    return m_minAreaThreshold / 100.0;
}

auto RegionFinderOptionsDialog::scaleFactor() const -> double
{
    return m_scaleFactor / 100.0;
}
