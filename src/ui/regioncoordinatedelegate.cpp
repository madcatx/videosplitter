#include "regioncoordinatedelegate.h"

#include <QSpinBox>

#include <cassert>

auto RegionCoordinateDelegate::createEditor(QWidget *parent, const QStyleOptionViewItem &, const QModelIndex &) const -> QWidget *
{
    auto editor = new QSpinBox{parent};
    editor->setRange(0, 10000); // TODO: Set range according to video dimensions

    connect(editor, static_cast<void (QSpinBox::*)(int)>(&QSpinBox::valueChanged), this, &RegionCoordinateDelegate::commiter);
    return editor;
}

void RegionCoordinateDelegate::commiter()
{
    auto editor = qobject_cast<QSpinBox *>(sender());

    emit commitData(editor);
}

auto RegionCoordinateDelegate::setEditorData(QWidget *editor, const QModelIndex &idx) const -> void
{
    QSpinBox *spinBox = qobject_cast<QSpinBox *>(editor);
    assert(spinBox != nullptr);

    spinBox->setValue(idx.data().toInt());
}
