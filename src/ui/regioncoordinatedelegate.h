#ifndef REGIONCOORDINATEDELEGATE_H
#define REGIONCOORDINATEDELEGATE_H

#include <QStyledItemDelegate>

class RegionCoordinateDelegate : public QStyledItemDelegate {
    Q_OBJECT

public:
    using QStyledItemDelegate::QStyledItemDelegate;

    auto createEditor(QWidget *parent, const QStyleOptionViewItem &option, const QModelIndex &idx) const -> QWidget * override;
    auto setEditorData(QWidget *editor, const QModelIndex &idx) const -> void;

private slots:
    void commiter();
};

#endif // REGIONCOORDINATEDELEGATE_H
