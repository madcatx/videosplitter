#include "controlwidget.h"
#include "ui_controlwidget.h"

#include <cmduserparams.h>
#include <ui/regioncoordinatedelegate.h>
#include <regionmanager.h>
#include <regiontablemodel.h>

#include <QFileDialog>
#include <QFileInfo>
#include <QMessageBox>

#include <cassert>

ControlWidget::ControlWidget(const CmdUserParams &userParams, RegionManager &mgr, QWidget *parent) :
    QWidget{parent},
    ui{new Ui::ControlWidget},
    h_regionMgr{mgr}
{
    ui->setupUi(this);

    auto model = new RegionTableModel{h_regionMgr, this};
    ui->qtvb_regions->setModel(model);
    ui->qtvb_regions->setSelectionMode(QAbstractItemView::SingleSelection);
    ui->qtvb_regions->setItemDelegate(new RegionCoordinateDelegate{this});

    m_loadVideoDlg = new QFileDialog{this, "Select a video file", qApp->applicationDirPath()};
    m_loadVideoDlg->setFileMode(QFileDialog::ExistingFile);
    m_loadVideoDlg->setAcceptMode(QFileDialog::AcceptOpen);
#ifdef USE_QT_DIALOGS
    m_loadVideoDlg->setOption(QFileDialog::DontUseNativeDialog, true);
#endif // USE_QT_DIALOGS
    m_loadVideoDlg->setNameFilters({"Video files (*.avi *.mp4 *.mkv)", "All files (*.*)"});

    m_saveOutputDlg = new QFileDialog{this, "Select output directory", qApp->applicationDirPath()};
    m_saveOutputDlg->setFileMode(QFileDialog::Directory);
#ifdef USE_QT_DIALOGS
    m_saveOutputDlg->setOption(QFileDialog::DontUseNativeDialog, true);
#endif // USE_QT_DIALOGS
#ifndef Q_OS_WIN32
    m_saveOutputDlg->setAcceptMode(QFileDialog::AcceptSave);
#endif // Q_OS_WIN32
    m_saveOutputDlg->setOption(QFileDialog::ShowDirsOnly, true);

    ui->qcbox_container->addItem("Single images", OutputOptions::SingleImages);
    ui->qcbox_container->addItem("AVI", OutputOptions::AVI);
    ui->qcbox_container->addItem("MP4", OutputOptions::MP4);
    ui->qcbox_container->addItem("Matroska", OutputOptions::MKV);
    setOutputMode();

    connect(ui->qpb_browseOutput, &QPushButton::clicked, this, &ControlWidget::onBrowseOutputClicked);
    connect(ui->qpb_browseVideo, &QPushButton::clicked, this, &ControlWidget::onBrowseVideoClicked);
    connect(ui->qpb_load, &QPushButton::clicked,
            [this] {
                emit loadVideo(ui->qle_videoFile->text());
            });
    connect(ui->qpb_removeRegion, &QPushButton::clicked,
            [this]() {
                const auto &indices = ui->qtvb_regions->selectionModel()->selectedIndexes();
                if (indices.empty())
                    return;
                h_regionMgr.removeRegion(indices.constFirst().row());
            });
    connect(ui->qpb_sortRegions, &QPushButton::clicked,
            [this]() {
                h_regionMgr.sort();
            });
    connect(ui->qcbox_container, static_cast<void (QComboBox::*)(int)>(&QComboBox::currentIndexChanged),
            [this]() { this->setOutputMode(); });
    connect(ui->qpb_split, &QPushButton::clicked, this, &ControlWidget::onSplitVideoClicked);
    connect(ui->qcb_autoFps, &QCheckBox::toggled,
            [this](const bool checked) {
                ui->qspb_fps->setDisabled(checked);
            });
    connect(ui->qpb_find, &QPushButton::clicked, [this]() { emit findRegions(); });
    connect(ui->qcb_autoNamingScheme, &QCheckBox::toggled, [this](const bool checked) { ui->qle_namingScheme->setEnabled(!checked); });

    applyUserParams(userParams);
}

ControlWidget::~ControlWidget()
{
    delete ui;
}

auto ControlWidget::applyUserParams(const CmdUserParams &params) -> void
{
    ui->qle_videoFile->setText(params.input);
    ui->qle_outputPath->setText(params.output);
    if (params.namingScheme.isEmpty())
        ui->qcb_autoNamingScheme->setChecked(true);
    else
        ui->qle_namingScheme->setText(params.namingScheme);

    if (params.container.isSet()) {
        const auto cnt = cmdCntToOpt(params.container());
        selectContainer(cnt);

        if (params.codec.isSet()) {
            const auto isVideo = cmdCodecIsVideo(params.codec());
            if (isVideo && cnt != OutputOptions::SingleImages)
                selectCodec(cmdCodecToVid(params.codec()));
            else if (!isVideo && cnt == OutputOptions::SingleImages)
                selectCodec(cmdCodecToImg(params.codec()));
            else {
                QMessageBox::warning(
                    this,
                    tr("Invalid initial parameters"),
                    tr("Requested output container and codec are not compatible")
                );
            }
        }
    }

    if (params.wellsToDirs) {
        if (ui->qcbox_container->currentData().value<OutputOptions::Containers>() == OutputOptions::SingleImages)
            ui->qcb_wellsToDirs->setChecked(params.wellsToDirs);
    }
}

template <typename T>
auto ControlWidget::currentCodec() -> T
{
    auto data = ui->qcbox_codec->currentData();
    assert(data.canConvert<T>());

    return data.value<T>();
}

void ControlWidget::onBrowseOutputClicked()
{
    const auto current = ui->qle_outputPath->text();
    if (!current.isEmpty()) {
        QFileInfo finfo{current};
        if (finfo.exists())
            m_saveOutputDlg->setDirectory(finfo.isDir() ? finfo.absoluteFilePath() : finfo.absolutePath());
    }

    if (m_saveOutputDlg->exec() == QDialog::Accepted) {
        const auto path = m_saveOutputDlg->selectedFiles().constFirst();
        ui->qle_outputPath->setText(path);
    }
}

void ControlWidget::onBrowseVideoClicked()
{
    const auto current = ui->qle_videoFile->text();
    if (!current.isEmpty()) {
        QFileInfo finfo{current};
        if (finfo.exists())
            m_loadVideoDlg->setDirectory(finfo.isDir() ? finfo.absoluteFilePath() : finfo.absolutePath());
    }

    if (m_loadVideoDlg->exec() == QDialog::Accepted) {
        const auto path = m_loadVideoDlg->selectedFiles().constFirst();
        ui->qle_videoFile->setText(path);
        emit loadVideo(path);
    }
}

void ControlWidget::onSplitVideoClicked()
{
    const auto path = ui->qle_outputPath->text();
    const auto scheme = ui->qle_namingScheme->text();
    const auto autoScheme = ui->qcb_autoNamingScheme->isChecked();

    if (path.isEmpty()) {
        QMessageBox::warning(this, "Invalid parameters", "No output path is set");
        return;
    }
    if (scheme.isEmpty() && !autoScheme) {
        QMessageBox::warning(this, "Invalid parameters", "No name prefix is set");
        return;
    }

    auto cnt = ui->qcbox_container->currentData().value<OutputOptions::Containers>();
    auto codecs = [this, cnt]() {
        if (cnt == OutputOptions::SingleImages)
            return std::make_tuple(currentCodec<OutputOptions::ImageCodecs>(), OutputOptions::MJPEG);
        else
            return std::make_tuple(OutputOptions::JPEG, currentCodec<OutputOptions::VideoCodecs>());
    }();

    const auto fps = ui->qcb_autoFps->isChecked() ? -1 : ui->qspb_fps->value();
    emit splitVideo({path,
                     autoScheme ? QString{} : scheme,
                     cnt,
                     std::get<0>(codecs),
                     std::get<1>(codecs),
                     fps,
                     ui->qcb_wellsToDirs->isChecked()});
}

auto ControlWidget::outputPath() const -> QString
{
    return ui->qle_outputPath->text();
}

template <typename T>
auto ControlWidget::selectCodec(const T codec) -> void
{
    const auto model = ui->qcbox_codec->model();

    for (int row{0}; row < model->rowCount(); row++) {
        const auto data = model->data(model->index(row, 0), Qt::UserRole);
        assert(data.canConvert<T>());

        if (data.value<T>() == codec) {
            ui->qcbox_codec->setCurrentIndex(row);
            return;
        }
    }

    assert(false);
}
template auto ControlWidget::selectCodec<OutputOptions::ImageCodecs>(OutputOptions::ImageCodecs) -> void;
template auto ControlWidget::selectCodec<OutputOptions::VideoCodecs>(OutputOptions::VideoCodecs) -> void;

auto ControlWidget::selectContainer(const OutputOptions::Containers cnt) -> void
{
    const auto model = ui->qcbox_container->model();

    for (int row{0}; row < model->rowCount(); row++) {
        const auto data = model->data(model->index(row, 0), Qt::UserRole);
        assert(data.canConvert<OutputOptions::Containers>());

        if (data.value<OutputOptions::Containers>() == cnt) {
            ui->qcbox_container->setCurrentIndex(row);
            setOutputMode();
            return;
        }
    }

    assert(false);
}

auto ControlWidget::setOutputMode() -> void
{
    auto cnt = ui->qcbox_container->currentData().value<OutputOptions::Containers>();

    ui->qcbox_codec->clear();
    if (cnt == OutputOptions::SingleImages) {
        ui->qcb_wellsToDirs->setEnabled(true);
        ui->qcbox_codec->addItem("JPEG", OutputOptions::JPEG);
        ui->qcbox_codec->addItem("PNG", OutputOptions::PNG);
    } else {
        ui->qcb_wellsToDirs->setEnabled(false);
        ui->qcbox_codec->addItem("MJPEG", OutputOptions::MJPEG);
        ui->qcbox_codec->addItem("XVID", OutputOptions::XVID);
        ui->qcbox_codec->addItem("H264", OutputOptions::H264);
        ui->qcbox_codec->addItem("H265", OutputOptions::H265);
    }
}

auto ControlWidget::wellsToDirs() const -> bool
{
    return ui->qcb_wellsToDirs->isChecked();
}
