#include "inprogressdialog.h"
#include "ui_inprogressdialog.h"

InProgressDialog::InProgressDialog(const QString &text, QWidget *parent) :
    QDialog{parent},
    ui{new Ui::InProgressDialog}
{
    ui->setupUi(this);
    setWindowTitle("Working");
    ui->ql_text->setText(text);
}

InProgressDialog::~InProgressDialog()
{
    delete ui;
}

auto InProgressDialog::reject() -> void
{
    // NOOP
}
