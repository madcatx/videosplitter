#include "runinferpositionsdialog.h"
#include "ui_runinferpositionsdialog.h"

#include <cmduserparams.h>
#include <positioninferencerunner.h>
#include <runtimeconfiguration.h>
#include <ui/inprogressdialog.h>

#include <QEventLoop>
#include <QMessageBox>
#include <QProcess>
#include <QSettings>
#include <QtConcurrent>

inline
auto checkExeExists(const QString &path) {
#ifdef Q_OS_WIN
    static const QChar SEP{';'};
#else
    static const QChar SEP{':'};
#endif // Q_OS_WIN

    if (QFileInfo{path}.exists())
        return true;

    const auto syspath = getenv("PATH");
    for (const auto &prefix : QString{syspath}.split(SEP)) {
        if (QFileInfo{prefix + "/" + path}.exists())
            return true;
    }

    return false;
}

RunInferPositionsDialog::RunInferPositionsDialog(const CmdUserParams &userParams, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::RunInferPositionsDialog)
{
    ui->setupUi(this);

    ui->ble_scriptPath->setAcceptMode(QFileDialog::AcceptOpen);
    ui->ble_scriptPath->setNameFilters({ "Python scripts (*.py)", "All files (*.*)" });

    ui->ble_modelPath->setAcceptMode(QFileDialog::AcceptOpen);
    ui->ble_modelPath->setFileMode(QFileDialog::Directory);
    ui->ble_modelPath->setOption(QFileDialog::ShowDirsOnly, true);

    ui->ble_imagesPath->setAcceptMode(QFileDialog::AcceptOpen);
    ui->ble_imagesPath->setFileMode(QFileDialog::Directory);
    ui->ble_imagesPath->setOption(QFileDialog::ShowDirsOnly, true);

    ui->ble_outputDir->setAcceptMode(QFileDialog::AcceptOpen);
    ui->ble_outputDir->setFileMode(QFileDialog::Directory);
    ui->ble_outputDir->setOption(QFileDialog::ShowDirsOnly, true);

    connect(ui->buttonBox, &QDialogButtonBox::accepted, this, &RunInferPositionsDialog::onAccepted);
    connect(ui->qcb_saveOutputImgDir, &QCheckBox::toggled, [this](const bool checked) {
        ui->ble_outputDir->setEnabled(!checked);
    });

    auto cfg = RuntimeConfiguration::get();
    const auto lastScriptPath = cfg->inferPositionsScriptPath();
    const auto lastModelPath = cfg->inferPositionsModelPath();

    if (QFileInfo{lastScriptPath}.exists())
        ui->ble_scriptPath->setPath(lastScriptPath);
    if (QFileInfo{lastModelPath}.exists())
        ui->ble_modelPath->setPath(lastModelPath);

    applyUserParams(userParams);
}

RunInferPositionsDialog::~RunInferPositionsDialog()
{
    delete ui;
}

auto RunInferPositionsDialog::applyUserParams(const CmdUserParams &params) -> void
{
    if (!params.posInfScript.isEmpty())
        ui->ble_scriptPath->setPath(params.posInfScript);
    if (!params.posInfModel.isEmpty())
        ui->ble_modelPath->setPath(params.posInfModel);

    if (params.posInfOutput.isEmpty())
        ui->qcb_saveOutputImgDir->setChecked(true);
    else
        ui->ble_outputDir->setPath(params.posInfOutput);
}

void RunInferPositionsDialog::onAccepted()
{
    const auto python3Path = RuntimeConfiguration::get()->python3Path();

    if (!checkExeExists(python3Path)) {
        QMessageBox::warning(this, "Cannot run inference script", "Path to python3 runtime is invalid");
        return;
    }

    if (ui->ble_imagesPath->path().isEmpty()) {
        QMessageBox::warning(this, "Cannot run inference script", "Images path is empty");
        return;
    }

    const auto outputToImgDir = ui->qcb_saveOutputImgDir->isChecked();
    if (ui->ble_outputDir->path().isEmpty() && !outputToImgDir) {
        QMessageBox::warning(this, "Cannot run inference script", "Output path is empty");
        return;
    }

    InProgressDialog dlg{"Running inference script", this};
    QFutureWatcher<QProcess::ExitStatus> watcher{};
    QEventLoop loop{};

    dlg.setModal(true);
    connect(&watcher, &QFutureWatcher<void>::started, &dlg, &InProgressDialog::show);
    connect(&watcher, &QFutureWatcher<void>::finished, &dlg, &InProgressDialog::accept);
    connect(&watcher, &QFutureWatcher<void>::finished, &loop, &QEventLoop::quit);

    const auto subdirs = ui->qcb_imagesInSubdirs->isChecked();
    auto execFunc = [this, subdirs, &python3Path]() {
        PositionInferenceRunner runner{
            ui->ble_imagesPath->path(),
            ui->qcb_saveOutputImgDir->isChecked() ? "" : ui->ble_outputDir->path(),
            ui->ble_scriptPath->path(),
            ui->ble_modelPath->path(),
            python3Path
        };

        if (subdirs)
            return runner.subdirs();
        return runner.singleDir();
    };

    auto fut = QtConcurrent::run(execFunc);
    watcher.setFuture(fut);

    loop.exec();

    try {
        auto ret = fut.result();
        if (ret != QProcess::NormalExit)
            QMessageBox::warning(this, "Analysis error", "Analysis process did not finish successfully");
        else {
            auto cfg = RuntimeConfiguration::get();
            cfg->set_inferPositionsScriptPath(ui->ble_scriptPath->path());
            cfg->set_inferPositionsModelPath(ui->ble_modelPath->path());

            accept();
        }
    } catch (const QException &ex) {
        QMessageBox::warning(this, "Analysis error", QString{"Unexpected error occured during analysis: %1"}.arg(ex.what()));
    }
}

auto RunInferPositionsDialog::setImagesInSubdirs(const bool on) -> void
{
    ui->qcb_imagesInSubdirs->setChecked(on);
}

auto RunInferPositionsDialog::setImagesPath(const QString &path) -> void
{
    ui->ble_imagesPath->setPath(path);
}
