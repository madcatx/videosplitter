#include "videoframelabel.h"

#include <QMouseEvent>
#include <QResizeEvent>

#include <cmath>

VideoFrameLabel::VideoFrameLabel(QWidget *parent) :
    QLabel{parent}
{
}

auto VideoFrameLabel::mouseMoveEvent(QMouseEvent *ev) -> void
{
    if (!m_selectionOn)
        return;
    emit regionMarked(scaleToSource({m_selectionFrom, ev->pos()}));
}

auto VideoFrameLabel::mousePressEvent(QMouseEvent *ev) -> void
{
    if (ev->button() == Qt::LeftButton) {
        m_selectionOn = true;
        m_selectionFrom = ev->pos();
    } else if (ev->button() == Qt::RightButton) {
        if (!m_selectionOn)
            emit removeRegion(scaleToSource(ev->pos()));
    }
}

auto VideoFrameLabel::mouseReleaseEvent(QMouseEvent *ev) -> void
{
    if (ev->button() == Qt::LeftButton) {
        if (m_selectionOn) {
            emit regionUnmarked();
            emit regionSelected(scaleToSource({m_selectionFrom, ev->pos()}));
            m_selectionOn = false;
        }
    }
}

auto VideoFrameLabel::resizeEvent(QResizeEvent *ev) -> void
{
    this->setPixmap(m_picture.scaled(ev->size()));
}

auto VideoFrameLabel::scaleToSource(const QPoint &pt) -> QPoint
{
    const float ow = m_picture.width();
    const float oh = m_picture.height();
    const float w = this->width();
    const float h = this->height();

    const int x = int(std::round(pt.x() * ow / w));
    const int y = int(std::round(pt.y() * oh / h));
    return {x, y};
}

auto VideoFrameLabel::scaleToSource(const QRect &rect) -> QRect
{
    auto topLeft = scaleToSource(rect.topLeft());
    auto bottomRight = scaleToSource(rect.bottomRight());

    return {topLeft, bottomRight};
}

auto VideoFrameLabel::setPicture(QPixmap pixmap) -> void
{
    m_picture = std::move(pixmap);
    this->setPixmap(m_picture.scaled(this->size()));
}
