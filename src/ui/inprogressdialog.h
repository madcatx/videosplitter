#ifndef INPROGRESSDIALOG_H
#define INPROGRESSDIALOG_H

#include <QDialog>

namespace Ui {
class InProgressDialog;
}

class InProgressDialog : public QDialog {
    Q_OBJECT

public:
    explicit InProgressDialog(const QString &text, QWidget *parent = nullptr);
    ~InProgressDialog();

protected:
    auto reject() -> void override;

private:
    Ui::InProgressDialog *ui;
};

#endif // INPROGRESSDIALOG_H
