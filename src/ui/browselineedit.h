#ifndef BROWSELINEEDIT_H
#define BROWSELINEEDIT_H

#include <QFileDialog>
#include <QWidget>

namespace Ui {
class BrowseLineEdit;
}

class BrowseLineEdit : public QWidget {
    Q_OBJECT

public:
    explicit BrowseLineEdit(QWidget *parent = nullptr);
    ~BrowseLineEdit();
    auto path() const -> QString;
    auto setAcceptMode(const QFileDialog::AcceptMode mode) -> void;
    auto setFileMode(const QFileDialog::FileMode mode) -> void;
    auto setNameFilters(const QStringList &filters) -> void;
    auto setOption(const QFileDialog::Option option, const bool on = true) -> void;
    auto setOptions(const QFileDialog::Options options) -> void;
    auto setPath(const QString &path) -> void;

private:
    Ui::BrowseLineEdit *ui;
    QFileDialog *m_dlg;
};

#endif // BROWSELINEEDIT_H
