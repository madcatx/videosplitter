#ifndef REGIONFINDEROPTIONSDIALOG_H
#define REGIONFINDEROPTIONSDIALOG_H

#include <QDialog>

namespace Ui {
class RegionFinderOptionsDialog;
}

class CmdUserParams;

class RegionFinderOptionsDialog : public QDialog {
    Q_OBJECT

public:
    explicit RegionFinderOptionsDialog(const CmdUserParams &userParams, QWidget *parent = nullptr);
    ~RegionFinderOptionsDialog();

    auto maxAreaThreshold() const -> double;
    auto minAreaThreshold() const -> double;
    auto scaleFactor() const -> double;

private:
    auto applyUserParams(const CmdUserParams &params) -> void;

    Ui::RegionFinderOptionsDialog *ui;

    double m_minAreaThreshold;
    double m_maxAreaThreshold;
    double m_scaleFactor;
};

#endif // REGIONFINDEROPTIONSDIALOG_H
