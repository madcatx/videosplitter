#include "configurationdialog.h"
#include "ui_configurationdialog.h"

#include <cmduserparams.h>
#include <runtimeconfiguration.h>

ConfigurationDialog::ConfigurationDialog(const CmdUserParams &userParams, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::ConfigurationDialog)
{
    ui->setupUi(this);

    ui->ble_python3Path->setAcceptMode(QFileDialog::AcceptOpen);
    ui->ble_python3Path->setWindowTitle("Select path to Python 3 executable");
#ifdef Q_OS_WIN
    ui->ble_python3Path->setNameFilters({ "Executable files (*.exe)", "All files (*.*)" });
#endif // Q_OS_WIN

    fillFromConfiguration();

    connect(ui->buttonBox, &QDialogButtonBox::accepted, this, &ConfigurationDialog::onAccepted);
    connect(ui->buttonBox, &QDialogButtonBox::rejected, this, &ConfigurationDialog::onRejected);

    applyUserParams(userParams);
}

ConfigurationDialog::~ConfigurationDialog()
{
    delete ui;
}

auto ConfigurationDialog::applyUserParams(const CmdUserParams &params) -> void
{
    if (!params.python3Path.isEmpty())
        ui->ble_python3Path->setPath(params.python3Path);

    auto cfg = RuntimeConfiguration::get();
    cfg->set_python3Path(ui->ble_python3Path->path());
}

auto ConfigurationDialog::fillFromConfiguration() -> void
{
    auto cfg = RuntimeConfiguration::get();

    ui->ble_python3Path->setPath(cfg->python3Path());
}

void ConfigurationDialog::onAccepted()
{
    auto cfg = RuntimeConfiguration::get();

    cfg->set_python3Path(ui->ble_python3Path->path());

    accept();
}

void ConfigurationDialog::onRejected()
{
    fillFromConfiguration();

    reject();
}

