#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <opencvvideo.h>
#include <outputoptions.h>
#include <regionmanager.h>
#include <ui/playbackwidget.h>

#include <QMainWindow>

#include <memory>

class CmdUserParams;
class QFileDialog;
class RegionFinderOptionsDialog;
class RunInferPositionsDialog;

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow {
    Q_OBJECT

public:
    MainWindow(const CmdUserParams &userParams, QWidget *parent = nullptr);
    ~MainWindow();

private:
    auto applyUserParams(const CmdUserParams &params) -> void;
    auto loadRegions(const QString &path) -> void;
    auto setCurrentRegions(const QString &path) -> void;
    auto setTitle() -> void;

    Ui::MainWindow *ui;

    RegionManager m_regionMgr;
    std::shared_ptr<OpenCVVideo> m_video;
    PlaybackWidget *m_playback;
    RegionFinderOptionsDialog *m_regFindOptsDlg;
    RunInferPositionsDialog *m_runInferPosDlg;

    QFileDialog *m_loadSaveRegionsDlg;
    QString m_currentRegionsPath;

private slots:
    void onFindRegions();
    void onLoadVideo(const QString &path);
    void onLoadRegions();
    void onSaveRegions();
    void onSaveRegionsAs();
    void onSplitVideo(const OutputOptions::Config &config);
};

#endif // MAINWINDOW_H
