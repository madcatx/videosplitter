#ifndef RUNINFERPOSITIONSDIALOG_H
#define RUNINFERPOSITIONSDIALOG_H

#include <QDialog>

namespace Ui {
class RunInferPositionsDialog;
}

class CmdUserParams;

class RunInferPositionsDialog : public QDialog {
    Q_OBJECT

public:
    explicit RunInferPositionsDialog(const CmdUserParams &userParams, QWidget *parent = nullptr);
    ~RunInferPositionsDialog();
    auto setImagesInSubdirs(const bool on) -> void;
    auto setImagesPath(const QString &path) -> void;

private:
    auto applyUserParams(const CmdUserParams &params) -> void;

    Ui::RunInferPositionsDialog *ui;

private slots:
    void onAccepted();
};

#endif // RUNINFERPOSITIONSDIALOG_H
