cmake_minimum_required(VERSION 3.5)

project(VideoSplitter LANGUAGES CXX)

option(USE_QT_DIALOGS "Use non-native Qt dialogs" OFF)

set(CMAKE_INCLUDE_CURRENT_DIR ON)

set(CMAKE_AUTOUIC ON)
set(CMAKE_AUTOMOC ON)
set(CMAKE_AUTORCC ON)

set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_STANDARD_REQUIRED ON)
set(CMAKE_CXX_EXTENSIONS OFF)

if (!MSVC)
    add_definitions("-Wall -Wextra -pedantic")
endif ()

find_package(Qt5Core REQUIRED)
find_package(Qt5Gui REQUIRED)
find_package(Qt5Widgets REQUIRED)
find_package(Qt5Concurrent REQUIRED)
find_package(OpenCV REQUIRED)

include_directories(${OpenCV_INCLUDE_DIRS}
                    ${CMAKE_CURRENT_SOURCE_DIR}/src)

set(VideoSplitter_SRCS
    src/cmdlineparser.cpp
    src/cmdlineparser.h
    src/cmduserparams.cpp
    src/cmduserparams.h
    src/globals.cpp
    src/globals.h
    src/headlessrunner.cpp
    src/headlessrunner.h
    src/main.cpp
    src/opencvvideo.cpp
    src/opencvvideo.h
    src/outputoptions.cpp
    src/outputoptions.h
    src/persistentsettings.cpp
    src/persistentsettings.h
    src/positioninferencerunner.cpp
    src/positioninferencerunner.h
    src/regionmanager.cpp
    src/regionmanager.h
    src/regiontablemodel.cpp
    src/regiontablemodel.h
    src/regionutil.cpp
    src/regionutil.h
    src/runtimeconfiguration.cpp
    src/runtimeconfiguration.h
    src/videoframe.cpp
    src/videoframe.h
    src/ui/browselineedit.cpp
    src/ui/browselineedit.h
    src/ui/browselineedit.ui
    src/ui/configurationdialog.cpp
    src/ui/configurationdialog.h
    src/ui/configurationdialog.ui
    src/ui/controlwidget.cpp
    src/ui/controlwidget.h
    src/ui/controlwidget.ui
    src/ui/mainwindow.cpp
    src/ui/mainwindow.h
    src/ui/mainwindow.ui
    src/ui/inprogressdialog.cpp
    src/ui/inprogressdialog.h
    src/ui/inprogressdialog.ui
    src/ui/playbackwidget.cpp
    src/ui/playbackwidget.h
    src/ui/playbackwidget.ui
    src/ui/regionfinderoptionsdialog.cpp
    src/ui/regionfinderoptionsdialog.h
    src/ui/regionfinderoptionsdialog.ui
    src/ui/regioncoordinatedelegate.cpp
    src/ui/regioncoordinatedelegate.h
    src/ui/runinferpositionsdialog.cpp
    src/ui/runinferpositionsdialog.h
    src/ui/runinferpositionsdialog.ui
    src/ui/videoframelabel.cpp
    src/ui/videoframelabel.h
    )

if (USE_QT_DIALOGS)
    add_definitions(-DUSE_QT_DIALOGS)
endif ()

if (WIN32)
    add_executable(VideoSplitter WIN32
                   ${VideoSplitter_SRCS}
                   )
else ()
    add_executable(VideoSplitter
                   ${VideoSplitter_SRCS}
                   )
endif ()

target_link_libraries(VideoSplitter
                      PRIVATE Qt5::Core
                      PRIVATE Qt5::Gui
                      PRIVATE Qt5::Widgets
                      PRIVATE Qt5::Concurrent
                      PRIVATE opencv_core
                      PRIVATE opencv_video
                      PRIVATE opencv_videoio
                      )
